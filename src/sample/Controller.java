package sample;

import com.itextpdf.text.Document;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfWriter;
import com.sun.scenario.effect.ImageData;
import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.concurrent.Task;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Bounds;
import javafx.geometry.Insets;
import javafx.geometry.Rectangle2D;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.*;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;

public class Controller implements ResizeableImageViews,Pages,PhotoViews {
    Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();
    Boolean isSaved =false;
    @FXML
    MenuBar menuBar = new MenuBar();
    @FXML
    FlowPane flowPane = new FlowPane();
    @FXML
    ScrollPane scrPane = new ScrollPane();

    @FXML
    Slider borderSlider;
    @FXML
    ColorPicker borderColorPicker;
    @FXML
    Button allImages;


    FileChooser fc = new FileChooser();

    @FXML
    MenuItem addClipArt;


    @FXML
    VBox frameBox;
    private boolean wizard;


    int paneWidth ;

    public void setPaneWidth(int paneWidth) {
        this.paneWidth = paneWidth;
    }

    public void setPaneHeight(int paneHeight) {
        this.paneHeight = paneHeight;
    }

    int paneHeight ;

    public int getPaneWidth() {
        return paneWidth;
    }

    public int getPaneHeight() {
        return paneHeight;
    }

    @FXML
    private Label numberOfImages = new Label();


    @FXML
     Group groupLeft = new Group();
    @FXML
     Group groupRight = new Group();
    @FXML
    SplitPane splitPane = new SplitPane();

    @FXML
    VBox dummyViewBox = new VBox();

    @FXML
    ButtonBar buttonBar = new ButtonBar();
    @FXML
    TextField pages = new TextField();
    @FXML
    StackPane stackPane = new StackPane();

    private boolean isAutomated = false;
    private int margin = 40;
    private int automatPaneNumber=0;
    private int numberOfPages = 0;
    private int currentPage = 0;
    private  int groupPage = 0;
    private ShapeCreator shapeCreator;



    int imageID=0;
    ArrayList<ThemeDisplay> tempThemes = new ArrayList<>();
    ArrayList<String > groups = new ArrayList<>();
    String saveFileName=null;
    private double multiplier;

    private DoubleProperty  zoomFactor = new SimpleDoubleProperty();

    public DoubleProperty getZoomFactor() {
        return zoomFactor;
    }

    public void setZoomFactor(double zoomFactor) {
        this.zoomFactor.set(zoomFactor);
    }

    double orgSceneX, orgSceneY;
    double orgTranslateX, orgTranslateY;


    @FXML
    private void initialize(){
        /*DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
        Date date = new Date();
        timestamp= dateFormat.format(date);*/
        //anchorSize();



        if(!stackPane.getChildren().contains(splitPane)){
            stackPane.getChildren().add(splitPane);
        }



        try {
            dataInit();



        } catch (IOException e) {
            e.printStackTrace();
        }

        SplitPane.Divider divider = splitPane.getDividers().get(0);
        divider.positionProperty().addListener((observable, oldvalue, newvalue) -> divider.setPosition(0.5));

        multiplier=1/listOfPanes.get(0).getZoomFactor();
        addClipArt.setOnAction(event -> {
            addImageToClipArt();
        });

        afterInit();
    }

    private void afterInit()
    {

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                if(wizard){
                    try{
                        openWizard();
                    }
                    catch (Exception e){
                        System.out.println(e.toString());
                    }
                }
            }
        });

    }

    private void addImageToClipArt() {
        Stage stage = new Stage();

        fc.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Képek","*.png","*.jpeg","*.jpg","*.bmp")
        );
        List<File> list =
                fc.showOpenMultipleDialog(stage);
        if(list!=null) {
            for(File file : list){
                openFile(file);
            }

        }
    }
    private void openFile(File file) {
        try {
            String path = getClass().getResource("cliparts").toString();
            System.out.println(path);
            String fileName = file.getName();
            String name = fileName.substring(fileName.lastIndexOf("/") + 1, file.getName().length());
            path = path.replace("file:/", "");

            final File dir = new File(path+"/"+name);
            Files.copy(file.toPath(), dir.toPath(), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException ex) {
            System.out.println(ex.toString());
        }
    }

    private void anchorSize(){
        groupLeft.maxHeight(paneHeight);
        groupLeft.maxWidth(paneWidth);
        groupRight.maxHeight(paneHeight);
        groupRight.maxWidth(paneWidth);
    }

    private MyPane createNewPane(){
        MyPane p = new MyPane(paneWidth,paneHeight);
        p.setZoomFactor(zoomFactor.doubleValue());
        p.setBgColor(Color.WHITE);
        /*
        p.setStyle("-fx-background-color: white;"

        */
        p.setPrefSize(paneWidth,paneHeight);
        return p;
    }

    private void setTextFieldText(int current,int all){
        pages.setText((current-1)+"-"+(current)+" / "+all);
    }

    @FXML
    private void exit(){
        if(!isSaved){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Nem mentettél");
            alert.setHeaderText("Kilépés előtt mindig ments!");
            alert.setContentText("Mentve");

            alert.showAndWait();
            isSaved = true;
        }

        Platform.exit();
    }


    private void setNumberOfImages(){

        numberOfImages.setText("Képek száma: " + fileID.size());
    }
    @FXML
    public void fillImagePlane(){
        Stage stage = new Stage();
        fc.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Képek","*.png","*.jpeg","*.jpg","*.bmp")
        );
        List<File> list =
                fc.showOpenMultipleDialog(stage);

        flowPane.setHgap(10);
        flowPane.setVgap(10);

        if(list!=null) {
            for(File file : list){
                if(filePaths.contains(file.getPath())){
                    System.out.println("A kép a listában már szerepel!");
                }
                else {
                    filePaths.add(file.getPath());
                    fileID.add(imageID);
                    PhotoView photoView = new PhotoView(imageID);
                    photoView.setImportantVisibility(false);
                    imageID++;
                    //méretezés átnézése
                    Image image = new Image(file.toURI().toString());

                    if(image.getWidth() > paneWidth || image.getHeight() > paneHeight){
                        image = new Image(file.toURI().toString(),paneWidth*0.9,paneHeight*0.9,true,true,true);
                    }
                    photoView.setPathOfFile(file.getPath());
                    photoView.setPhoto(image);
                    photoViews.add(photoView);
                    photoView.setOnContextMenuRequested(event -> {
                        ImageViewContextMenu contextMenu = new ImageViewContextMenu(photoView);
                        contextMenu.show(photoView,event.getScreenX(),event.getScreenY());
                    });
                    flowPane.getChildren().add(photoView);
                    isAutomated=false;
                }
            }
            /*flowPane.setOrientation(Orientation.HORIZONTAL);
            scrPane.setContent(flowPane);
            scrPane.setFitToWidth(true);
            scrPane.setFitToHeight(true);*/
        }
        setNumberOfImages();

    }
    private class ImageViewContextMenu extends ContextMenu {
        public ImageViewContextMenu(PhotoView iv) {
            MenuItem deleteImageView = new MenuItem("Kép törlése");
            deleteImageView.setOnAction(event -> {

                deleteImage(iv);


            });

            MenuItem addToGroup = new MenuItem("Hozzáadás csoporthoz");
            addToGroup.setOnAction(event -> {
                try {
                    addToGroup(iv);
                }
                catch (Exception e){
                    System.out.println("Hiba a csoportnál");
                    System.out.println(e.toString());
                }

            });
            MenuItem info = new MenuItem("info");
            info.setOnAction(event -> {
                System.out.println(iv.getPhotoGroup());
            });



            getItems().addAll(addToGroup,new SeparatorMenuItem(),deleteImageView);
            //getItems().add(resize);
        }
    }
    public void addTwoPanes(){
        listOfPanes.add(createNewPane());
        listOfPanes.add(createNewPane());
        numberOfPages+=2;
        setTextFieldText(currentPage,numberOfPages);
    }
    public void deleteTwoPanes(){
        while(currentPage==listOfPanes.size()-1 ||currentPage==listOfPanes.size()-2){
            changePagesDown();
        }
        if(listOfPanes.get(listOfPanes.size()-1).getMainPane().getChildren().toArray().length==0 || listOfPanes.get(listOfPanes.size()-2).getMainPane().getChildren().toArray().length==0){
            listOfPanes.remove(listOfPanes.size()-1);
            listOfPanes.remove(listOfPanes.size()-1);
        }
        else{
            Button cancel = new Button("Mégse");
            Button delete = new Button("Törlés");
            int p1=listOfPanes.size()-1,p2=listOfPanes.size();
            String alert = "Biztosan törli a "+ String.valueOf(p1)+". és a "+p2+". oldalt? Az oldalon vannak már képek!";
            Alert a =new Alert(Alert.AlertType.CONFIRMATION,alert,ButtonType.YES,ButtonType.CANCEL);
            a.setTitle("Oldalak törlése");
           a.showAndWait();
           if(a.getResult()==ButtonType.YES){
               listOfPanes.remove(listOfPanes.size()-1);
               listOfPanes.remove(listOfPanes.size()-1);
               changePagesDown();
           }

        }

        numberOfPages-=2;
        setTextFieldText(currentPage,numberOfPages);
    }
    public void changePagesUp(){
        if(listOfPanes.size()>currentPage){
            currentPage+=2;
            groupLeft.getChildren().clear();
            groupRight.getChildren().clear();
            groupLeft.getChildren().add(listOfPanes.get(currentPage-2));
            groupRight.getChildren().add(listOfPanes.get(currentPage-1));
            setTextFieldText(currentPage,numberOfPages);
        }


    }
    public void changePagesDown(){
        if(2<currentPage){
            currentPage-=2;
            groupLeft.getChildren().clear();
            groupRight.getChildren().clear();
            groupLeft.getChildren().add(listOfPanes.get(currentPage-2));
            groupRight.getChildren().add(listOfPanes.get(currentPage-1));
            setTextFieldText(currentPage,numberOfPages);
        }

    }
    private void deleteImage(PhotoView pv){
        fileID.remove(pv.getID());
        filePaths.remove(pv.getPathOfFile());
        flowPane.getChildren().remove(pv);
        photoViews.remove(pv);
        setNumberOfImages();

    }

    public void saveAs() {
        //C:\Users\User\Desktop\PhotoalbumMaker\src\sample\saves
        String path="src/sample/saves";
        if(saveFileName==null) {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("savefile.fxml"));
            Parent root = null;
            try {

                root = loader.load();
            } catch (Exception e) {
                System.out.println("Projekt mentési hiba!");
            }
            Savefile controller = loader.getController();

            loader.setRoot(Savefile.class);
            loader.setController(Savefile.class);

            Stage savestage = new Stage(StageStyle.UNDECORATED);

            savestage.initModality(Modality.APPLICATION_MODAL);
            savestage.setTitle("Projekt mentése");
            savestage.setScene(new Scene(root));

            savestage.showAndWait();
            String tmp = "";
            try {
                tmp = controller.getFileName();
            } catch (Exception e) {
                System.out.println("Nem adott mentésnek nevet!");
            }

            if (!tmp.equals(null)) {
                saveFileName = tmp;
            }

        }

        if(saveFileName!=null){


            File file = new File(path+"/"+saveFileName+".save");
            if(!file.exists())
            {
                try {
                    file.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(file.exists()){
                //System.out.println("Fájl kész!");
                try (BufferedWriter bw = new BufferedWriter(new FileWriter(file, false))) {
                    bw.write("[PANEWIDTH]"+getPaneWidth()+"\n[PANEHEIGHT]"+getPaneHeight()+"\n");
                    bw.write("[PHOTOVIEWS]");
                    bw.newLine();
                    for(PhotoView pv : photoViews){
                        bw.write(pv.saveAs());
                        bw.newLine();
                    }
                    bw.write("[PHOTOVIEWS/]");
                    bw.newLine();
                    bw.write("[MYPANES]\n");


                    for(MyPane mp :listOfPanes){
                        bw.write("[PANE]\n");
                        bw.write("[BORDER]"+mp.getBorderColor()+";"+mp.getBorderWidth()+";"+mp.getBorderInsets()+"\n");
                        if(mp.getTheme()!= null)
                        bw.write("[BACKGROUND]"+mp.getTheme()+"\n");
                        else{
                            bw.write("[BACKGROUND]"+mp.getBgColor()+"\n");
                        }

                        mp.MainPane.getChildren().toArray();
                        for(Object o: mp.MainPane.getChildren().toArray()){
                            System.out.println("eddig ok"+o.toString());
                            if(o instanceof ResizeableImageView){
                                //System.out.println(o.toString());
                                bw.write("[IMAGEVIEW]\n"+((ResizeableImageView)o).saveAs());
                                Bounds imageBound = ((ResizeableImageView)o).getBoundsInParent();
                                System.out.println(imageBound.getMinX()+" "+imageBound.getMaxX()+ " "+ imageBound.getMinY()+" "+imageBound.getMaxY());
                                bw.write("[POSITION]"+(int)imageBound.getMinX()+";"+(int)imageBound.getMinY()+"\n");
                                bw.write("[SHAPE]"+((ResizeableImageView)o).getShapeType()+"\n");
                                bw.write("[NUM]"+listOfPanes.indexOf(mp)+"\n");
                                bw.write("[IMAGEVIEW/]\n");
                            }
                            if(o instanceof FloatingText){
                                bw.write("[TEXT]\n");
                                bw.write("[SAYS]"+((FloatingText) o).getText()+"\n");
                                bw.write("[FONTSIZE]"+(int)((FloatingText) o).getFont().getSize()+"\n");
                                bw.write("[FONT]"+((FloatingText) o).getFont().getName()+"\n");
                                bw.write("[COLOR]"+((FloatingText) o).getFill()+"\n");
                                Bounds textBound = ((FloatingText)o).getBoundsInParent();
                                bw.write("[POSITION]"+(int)textBound.getMinX()+";"+(int)textBound.getMaxY()+"\n");
                                bw.write("[NUM]"+listOfPanes.indexOf(mp)+"\n");
                                bw.write("[TEXT/]\n");
                            }
                            if(o instanceof FloatingTextBox){
                                bw.write("[TEXTBOX]\n");
                                bw.write("[SAYS]"+((FloatingTextBox) o).getText()+"\n");
                                bw.write("[FONTSIZE]"+(int)((FloatingTextBox) o).getFont().getSize()+"\n");
                                bw.write("[FONT]"+((FloatingTextBox)o).getFont().getName()+"\n");
                                //bw.write("[COLOR]"+((FloatingText) o).getFill()+"\n");
                                Bounds textBound = ((FloatingTextBox)o).getBoundsInParent();
                                bw.write("[POSITION]"+(int)textBound.getMinX()+";"+(int)textBound.getMinY()+"\n");
                                bw.write("[SIZE]"+((FloatingTextBox)o).getImage().getFitWidth()+";"+((FloatingTextBox)o).getImage().getFitHeight()+"\n");
                                bw.write("[MAXWIDTH]"+((FloatingTextBox)o).getMaxTextWidth()+"\n");
                                bw.write("[IMAGEPATH]"+((FloatingTextBox)o).getBgImagePath()+"\n");
                                bw.write("[NUM]"+listOfPanes.indexOf(mp)+"\n");
                                bw.write("[TEXTBOX/]\n");
                            }
                        }
                        bw.write("[PANE/]\n");
                    }

                    bw.write("[MYPANES/]");
                    bw.newLine();




                } catch (IOException e) {
                    e.printStackTrace();

                }
            }
        }
    }


    public void addToGroup(PhotoView iv) throws IOException{
        FXMLLoader loader = new FXMLLoader();

        loader.setLocation(getClass().getResource("photogroups.fxml"));
        Parent root = loader.load();


        PhotoGroups controller = loader.getController();
        controller.initData(iv);
        loader.setRoot(PhotoGroup.class);
        loader.setController(PhotoGroup.class);

        Stage groupstage = new Stage(StageStyle.DECORATED);

        groupstage.initModality(Modality.APPLICATION_MODAL);
        groupstage.setTitle("Csoporthoz hozzáadás");
        groupstage.setScene(new Scene(root));

        groupstage.showAndWait();

        if(!controller.getGroupOfImage().equals("")) {
            iv.setPhotoGroup(controller.getGroupOfImage());
            if(!groups.contains(controller.getGroupOfImage()))
            groups.add(controller.getGroupOfImage());
        }


    }

    private void dataInit() throws IOException {

            FXMLLoader loader = new FXMLLoader();

            loader.setLocation(getClass().getResource("datainit.fxml"));
            Parent root = loader.load();


            DataInit controller = loader.getController();
            //controller.initData(iv);
            loader.setRoot(DataInit.class);
            loader.setController(DataInit.class);

            Stage dataInitStage = new Stage(StageStyle.UNDECORATED);

            dataInitStage.initModality(Modality.APPLICATION_MODAL);
            dataInitStage.setTitle("Alapbeállítások");
            dataInitStage.setScene(new Scene(root));

            dataInitStage.showAndWait();

            if(controller.getNewFile().equals("create")){
                DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
                Date date = new Date();
                //timestamp= dateFormat.format(date);
                numberOfPages=numberOfPages+10;
                setPaneHeight(controller.getHeight());
                setPaneWidth(controller.getWidth());
                setZoomFactor(Math.min((625.0/(double)paneWidth),(700.0/(double)paneHeight)));
                //System.out.println(Math.min((625.0/(double)paneWidth),(700.0/(double)paneHeight)));
                for(int i = 0;i < numberOfPages;i++){
                    listOfPanes.add(createNewPane());
                }



                groupLeft.getChildren().add(listOfPanes.get(0));
                groupRight.getChildren().add(listOfPanes.get(1));
                currentPage+=2;
                setTextFieldText(currentPage,numberOfPages);

                if( controller.isWizardOn()==true){
                    System.out.println("wizard started");
                    wizard=true;

                }
                else{
                    wizard=false;
                }

            }
            else if(controller.getNewFile().equals("load")){
                saveFileName=controller.getSaveFile().replace(".save","");
                //System.out.println(timestamp);
                loadProject(controller.getSaveFile());
            }


            shapeCreator = new ShapeCreator();


    }

    private void openWizard() throws IOException {
        FXMLLoader loader = new FXMLLoader();

        loader.setLocation(getClass().getResource("wizard.fxml"));
        Parent root = loader.load();


        Wizard controller = loader.getController();
        //controller.initData(iv);
        loader.setRoot(Wizard.class);
        loader.setController(Wizard.class);
        controller.initData(paneHeight,paneWidth,multiplier);

        Stage wizardStage = new Stage(StageStyle.DECORATED);

        wizardStage.initModality(Modality.APPLICATION_MODAL);
        wizardStage.setTitle("Varázsló");
        wizardStage.setScene(new Scene(root));

        wizardStage.showAndWait();

        System.out.println(photoViews.size());
        if(photoViews.size()>0){
            System.out.println(photoViews.size());
            for(PhotoView pv:photoViews)
            {
                pv.setImportantVisibility(false);
                flowPane.getChildren().add(pv);
            }


        }
        groupLeft.getChildren().add(listOfPanes.get(0));
        groupRight.getChildren().add(listOfPanes.get(1));
        setTextFieldText(currentPage,listOfPanes.size());
        numberOfPages = listOfPanes.size();

    }

    private void loadProject(String filename){
        loadThemesBackGround();
        System.out.println("loading "+filename);
        String pathOfFile="src/sample/saves/"+filename;
        String path="";
        int id=0;
        boolean onpane=false;
        double fitW=0;
        double fitH=0;
        int x=0,y=0;
        int pageNum=0;
        try (BufferedReader reader = new BufferedReader(new FileReader(new File(pathOfFile)))) {

            String line;
            while ((line = reader.readLine()) != null) {


                 if(line.contains("[PANEWIDTH]")){
                    paneWidth=Integer.parseInt((line).replace("[PANEWIDTH]",""));
                    paneHeight=Integer.parseInt((line=reader.readLine()).replace("[PANEHEIGHT]",""));
                    setZoomFactor(Math.min((625.0/(double)paneWidth),(700.0/(double)paneHeight)));
                    line=reader.readLine();
                    }
                 else if (line.contains("[PHOTOVIEW]")) {
                        path = line = reader.readLine().replace("[PATH]", "file:/");

                        path = path.replace("\\", "/");
                        id = Integer.parseInt(line = reader.readLine().replace("[ID]", ""));
                        onpane = Boolean.parseBoolean(line = reader.readLine().replace("[ONPANE]", ""));


                        Image image = new Image(path);
                        File f = new File(path.replace("file:/",""));
                     System.out.println(path);
                        if (f.exists()){
                            fileID.add(id);
                            imageID=id;

                            if(image.getWidth() > paneWidth || image.getHeight() > paneHeight){
                                image = new Image(path,paneWidth-margin,paneHeight-margin,true,true,true);
                            }
                            filePaths.add(path);
                            PhotoView pv = new PhotoView(id);
                            //pv.setFitWidth(250);
                            //pv.setFitHeight(250);
                            //pv.setPreserveRatio(true);
                            if(pv!=null)
                                //pv.setPhoto(image);
                                pv.getPhotoView().setImage(image);
                            else
                                System.out.println("pv null");
                            pv.setOnPane(onpane);
                            path=path.replace("file:/","");
                            pv.setPathOfFile(path);
                            photoViews.add(pv);
                            pv.setImportantVisibility(false);
                            pv.setOnContextMenuRequested(event -> {
                                ImageViewContextMenu contextMenu = new ImageViewContextMenu(pv);
                                contextMenu.show(pv,event.getScreenX(),event.getScreenY());
                            });
                            flowPane.getChildren().add(pv);
                            line=reader.readLine();
                        }
                        else{
                            System.out.println("A kép nem található!");
                        }
                        /*System.out.println(path);
                        System.out.println(id);
                        System.out.println(onpane);*/

                        //Image image = new Image(path);


                    }

                  else if(line.contains("[PANE]")){
                        MyPane myPane = createNewPane();
                        double paneBorderWidth,borderInsets;

                        Color paneBorderColor;
                       listOfPanes.add(myPane);
                     String[] tmp = (line=reader.readLine().replace("[BORDER]","")).split(";");
                     paneBorderColor=(tmp[0].equals("null"))?null:Color.web(tmp[0]);
                     paneBorderWidth=Double.parseDouble(tmp[1]);
                     borderInsets=Double.parseDouble(tmp[2]);
                     myPane.setBorder(new Border(new BorderStroke(paneBorderColor,BorderStrokeStyle.SOLID,CornerRadii.EMPTY,new BorderWidths(paneBorderWidth,paneBorderWidth,paneBorderWidth,paneBorderWidth),new Insets(borderInsets,borderInsets,borderInsets,borderInsets))));
                     String backGround = line=reader.readLine().replace("[BACKGROUND]","");
                     Color bgColor;
                     if(backGround.contains("0x"))
                     {
                         bgColor = Color.web(backGround);
                         myPane.setBgColor(bgColor);
                         myPane.setTheme("null");
                     }
                     else{
                         myPane.setTheme(backGround);
                     }

                     myPane.setBorderWidth(paneBorderWidth);
                     myPane.setBorderInsets(borderInsets);
                     myPane.setBorderColor(paneBorderColor);

                        line=reader.readLine();

                     if(line.contains("[IMAGEVIEW]")) {

                         readImageView(line,reader);
                     }
                     if(line.contains("[TEXT]")){

                         readFloatingText(line,reader);
                     }
                     if(line.contains("[TEXTBOX]")){

                         readFloatingTextBox(line,reader);
                     }


                   }
                else if(line.contains("[IMAGEVIEW]")) {

                     readImageView(line,reader);

                }
                else if(line.contains("[TEXT]")){

                    readFloatingText(line,reader);
                }
                else if(line.contains("[TEXTBOX]")){

                    readFloatingTextBox(line,reader);
                }


                else if(line.contains("[PANE/]")){
                       pageNum++;

                   }






            }
            System.out.println("tempthemes size :" +tempThemes.size());
            System.out.println("pages size: "+listOfPanes.size());
            for(MyPane p : listOfPanes){
                for(ThemeDisplay td : tempThemes){
                    System.out.println(p.getTheme()+" "+td.getThemeName());
                    if(p.getTheme().equals(td.getThemeName())){
                        if (listOfPanes.indexOf(p) == 0) {
                            p.addBackGroundImageWithoutZoom(td.getFront());
                        } else if (listOfPanes.indexOf(p) == listOfPanes.size() - 1) {
                            p.addBackGroundImageWithoutZoom(td.getEnd());
                        } else {
                            p.addBackGroundImageWithoutZoom(td.getMain());
                        }
                    }
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        groupLeft.getChildren().add(listOfPanes.get(0));
        groupRight.getChildren().add(listOfPanes.get(1));
        currentPage+=2;
        numberOfPages=listOfPanes.size();
        setTextFieldText(currentPage,listOfPanes.size());
        setNumberOfImages();





    }


    private void loadThemesBackGround(){

                String path = getClass().getResource("themes").toString();
                //System.out.println(path);
                path = path.replace("file:/", "");
                final File dir = new File(path);
                final String[] EXTENSIONS = new String[]{"jpeg", "jpg", "png", "bmp"};
                final FilenameFilter IMAGE_FILTER = (dir1, name) -> {
                    for (final String ext : EXTENSIONS) {
                        if (name.endsWith("." + ext)) {
                            return (true);
                        }
                    }
                    return (false);
                };

                if (dir.isDirectory()) {

                    List<File> images = Arrays.asList(dir.listFiles(IMAGE_FILTER));



                    for(int i = 0; i < images.size(); i=i+6){
                        Image end = null;
                        Image front=null;
                        Image main=null;
                        Image endRotated = null;
                        Image frontRotated=null;
                        Image mainRotated=null;

                        if (images.get(i).toURI().toString().contains("End")) {
                            end = new Image(images.get(i).toURI().toString());
                        }
                        if (images.get(i + 2).toURI().toString().contains("Front")) {
                            front = new Image(images.get(i + 2).toURI().toString());
                        }
                        if (images.get(i + 4).toURI().toString().contains("Main")) {
                            main = new Image(images.get(i + 4).toURI().toString());
                        }

                        if(images.get(i+1).toURI().toString().contains("End") && images.get(i+1).toURI().toString().contains("Rotated")){
                            endRotated = new Image(images.get(i+1).toURI().toString());
                        }
                        if(images.get(i+3).toURI().toString().contains("Front") && images.get(i+3).toURI().toString().contains("Rotated")){
                            frontRotated = new Image(images.get(i+3).toURI().toString());
                        }
                        if(images.get(i+5).toURI().toString().contains("Main") && images.get(i+5).toURI().toString().contains("Rotated"))
                        {
                            mainRotated = new Image(images.get(i+5).toURI().toString());
                        }


                        /*System.out.println("0." +images.get(i).toURI().toString());
                        System.out.println("1." +images.get(i+1).toURI().toString());
                        System.out.println("2." +images.get(i+2).toURI().toString());
                        System.out.println("3." +images.get(i+3).toURI().toString());
                        System.out.println("4." +images.get(i+4).toURI().toString());
                        System.out.println("5." +images.get(i+5).toURI().toString());*/


                        ThemeDisplay themeDisplay = new ThemeDisplay(front,main,end);
                        themeDisplay.setRotated(false);



                        ThemeDisplay themeDisplayRotated = new ThemeDisplay(frontRotated,mainRotated,endRotated);
                        themeDisplayRotated.setRotated(true);




                        String temp =(images.get(i).toURI().toString().replace("End", "").replace("file:",""));
                        String[] temp2 = temp.split("/");

                        themeDisplay.setThemeName(temp2[temp2.length-1].replace(".png",""));
                        System.out.println(themeDisplay.getThemeName());
                        temp =(images.get(i+1).toURI().toString().replace("End", "").replace("file:",""));
                        temp2 = temp.split("/");
                        themeDisplayRotated.setThemeName(temp2[temp2.length-1].replace(".png","")+"Rotated");
                        System.out.println(themeDisplay.getThemeName());
                        tempThemes.add(themeDisplay);
                        tempThemes.add(themeDisplayRotated);
                    }
                }

                /*for (ThemeDisplay td : tempThemes) {
                    //System.out.println(td.getThemeName());
                    if(td.getThemeName().equals(listOfPanes.get(0).getTheme())){
                        for(MyPane p : listOfPanes)
                        {
                            if(!p.getTheme().equals("null")){
                                if (listOfPanes.indexOf(p) == 0) {
                                    p.addBackGroundImageWithoutZoom(td.getFront());
                                } else if (listOfPanes.indexOf(p) == listOfPanes.size() - 1) {
                                    p.addBackGroundImageWithoutZoom(td.getEnd());
                                } else {
                                    p.addBackGroundImageWithoutZoom(td.getMain());
                                }
                            }
                        }
                    }

                }*/





    }


    private void readFloatingTextBox(String line, BufferedReader reader) throws IOException {
        String text = line = reader.readLine().replace("[SAYS]","");
        double fontSize = Double.parseDouble(line=reader.readLine().replace("[FONTSIZE]",""));
        String fontType = line=reader.readLine().replace("[FONT]","");
        //Color fontColor = Color.web(line=reader.readLine().replace("[COLOR]",""));
        int x,y;
        String[] temp = (line=reader.readLine().replace("[POSITION]","")).split(";");
        x=Integer.parseInt(temp[0]);
        y=Integer.parseInt(temp[1]);
        String[] temp2 = (line=reader.readLine().replace("[SIZE]","")).split(";");
        double width=Double.parseDouble(temp2[0]);
        double height=Double.parseDouble(temp2[1]);
        int maxwidth =Integer.parseInt(line=reader.readLine().replace("[MAXWIDTH]",""));
        String imgpath = line=reader.readLine().replace("[IMAGEPATH]","");
        int num = Integer.parseInt(line=reader.readLine().replace("[NUM]",""));

        line = reader.readLine();

        File image = new File(imgpath);

        FloatingTextBox ft = new FloatingTextBox();
        ft.setText(text);
        ft.setBgImagePath(imgpath);
        System.out.println(imgpath);
        ft.setMaxTextWidth(maxwidth);
        System.out.println(imgpath);
        if(image.exists()){
            Image img = new Image(image.toURI().toString());
            ft.setImage(img);
        }

        //ft.setMultiplier(multiplier);
        //ft.setFontColor(fontColor);
        ft.getImage().setFitWidth(width);
        ft.getImage().setFitHeight(height);
        ft.setFontAndSize(fontType,(int)fontSize);
        placeFloatingTextBox(ft,x,y,num);
    }

    private void readFloatingText(String line,BufferedReader reader) throws IOException {


        String text = line = reader.readLine().replace("[SAYS]","");
        double fontSize = Double.parseDouble(line=reader.readLine().replace("[FONTSIZE]",""));
        String fontType = line=reader.readLine().replace("[FONT]","");
        Color fontColor = Color.web(line=reader.readLine().replace("[COLOR]",""));
        int x,y;
        String[] temp = (line=reader.readLine().replace("[POSITION]","")).split(";");
        x=Integer.parseInt(temp[0]);
        y=Integer.parseInt(temp[1]);
        int num = Integer.parseInt(line=reader.readLine().replace("[NUM]",""));

        line = reader.readLine();

        FloatingText ft = new FloatingText(text);
        //ft.setMultiplier(multiplier);
        ft.setFontColor(fontColor);
        ft.setFont(Font.font(fontType,fontSize));
        ft.setFontSize((int)fontSize);
        placeFloatingText(ft,x,y,num);


    }
    private void placeFloatingText(FloatingText ft,int x,int y,int page){
        listOfPanes.get(page).placeFloatingTextOnPane(ft,x,y);


    }
    private void placeFloatingTextBox(FloatingTextBox ft,int x,int y,int page){
        listOfPanes.get(page).placeFloatingTextBoxOnPane(ft,x,y);


    }
    private void readImageView(String line,BufferedReader reader) throws IOException {
        String path;
        double fitW=0;
        double fitH=0;
        int x=0,y=0;
        int pageNum=0;
        String shape;
        Color ivBorderColor;
        double borderWidth;
        shapeCreator = new ShapeCreator();

        path = line = reader.readLine().replace("[PATH]", "file:/");
        path = path.replace("\\", "/");
        fitW = Integer.parseInt(line = reader.readLine().replace("[WIDTH]", ""));
        fitH = Integer.parseInt(line = reader.readLine().replace("[HEIGHT]", ""));
        String[] tmp = (line=reader.readLine().replace("[BORDER]","")).split(";");
        ivBorderColor=(tmp[0].equals("null"))?null:Color.web(tmp[0]);
        borderWidth=Double.parseDouble(tmp[1]);
        String temp = line = reader.readLine().replace("[POSITION]", "");
        String[] temp2 = temp.split(";");
        x = Integer.parseInt(temp2[0]);
        y = Integer.parseInt(temp2[1]);
        shape=line =reader.readLine().replace("[SHAPE]","");
        int num = Integer.parseInt(line=reader.readLine().replace("[NUM]",""));


        Image image = new Image(path);



        ResizeableImageView iv = new ResizeableImageView(new Image(path));
        path=path.replace("file:/","");
        iv.setPathOfImage(path);
        if(path.contains("clipart")){
            iv.setImageView(false);
        }
        File f = new File(path);
        System.out.println(f.exists());
        if(ivBorderColor!=null){
            Border brdr = new Border(new BorderStroke(ivBorderColor, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(borderWidth,borderWidth,borderWidth,borderWidth), new Insets(0, 0, 0, 0)));
            iv.setBorder(brdr);
            iv.setBorderColor(ivBorderColor);
            iv.setBorderWidth(borderWidth);

        }
        if(image==null){

        }
        if(f.exists()){
            imageViewList.add(iv);
            placeIt(iv, num, x, y, null);
            iv.getImageView().setFitWidth(fitW);
            iv.getImageView().setFitHeight(fitH);
            System.out.println(shape);
            System.out.println(iv.toString());
            shapeCreator.addShapeToImageView(iv,shape);

        }
        else{
            System.out.println("Kép nem található!");
        }


        //System.out.println(path+"\n"+fitW+" "+fitH+" "+x+" "+y);
        line=reader.readLine();
    }

    @SuppressWarnings("Duplicates")
    public void groupLayout(){
        if(groups.size()>0){
            int centerX = (int)listOfPanes.get(0).getWidth()/2;
            int centerY = (int)listOfPanes.get(0).getHeight()/2;



            Random rando = new Random();
            int y=20;
            int x=20;
            for(int i = 0;i < groups.size();i++){

                String group = groups.get(i);
                FloatingText floatingText = new FloatingText(group);
                floatingText.setFontSize((int)multiplier*36);
                floatingText.setStyle("-fx-font-size: " + (multiplier * 36) + ";");
                int textX = (int)floatingText.getBoundsInLocal().getWidth()/2;
                int textY = (int)floatingText.getBoundsInLocal().getHeight()/2;

                if(!(listOfPanes.size()-1<groupPage)){
                    addTwoPanes();
                }
                listOfPanes.get(groupPage).placeFloatingTextOnPane(floatingText,centerX-textX,centerY-textY);


                groupPage++;

                for(PhotoView pv : photoViews){
                    if(!pv.isOnPane() && pv.getPhotoGroup()!=null && pv.getPhotoGroup().equals(group)){
                        System.out.printf(pv.pathOfFile+"\n");
                        ResizeableImageView iv = new ResizeableImageView(pv.getImage());
                        iv.setPathOfImage(pv.getPathOfFile());
                        /*if(iv.getImageView().getFitWidth() < paneWidth || iv.getImageView().getFitHeight() < paneHeight){
                            if(iv.getImageView().getImage().getHeight()>iv.getImageView().getImage().getWidth()){
                                iv.getImageView().setFitHeight(paneHeight-(paneHeight/10));
                            }
                            else{
                                iv.getImageView().setFitWidth(paneWidth-(paneWidth/10));
                            }


                        }
                        else{
                            iv.getImageView().setFitWidth(iv.getImageView().getImage().getWidth());
                            iv.getImageView().setFitHeight(iv.getImageView().getImage().getHeight());
                        }*/

                        int height = (int)iv.getImageView().getImage().getHeight();
                        int width = (int) iv.getImageView().getImage().getWidth();
                        int space = rando.nextInt(11)+10;
                        x =rando.ints(20,(int)(paneWidth-iv.getImageView().getFitWidth())+1).findFirst().getAsInt();
                        if(iv.getImageView().getFitWidth() < paneWidth && iv.getImageView().getFitHeight() < paneHeight){
                            if(!(groupPage < listOfPanes.size()-2))
                            {
                                addTwoPanes();
                            }
                            if(paneHeight > (y+space+height) )
                            {
                                placeIt(iv,groupPage,x,y,pv);
                                y=y+space+height;
                            }
                            else
                            {
                                if(groupPage < listOfPanes.size()-2)
                                {
                                    groupPage++;
                                    y = 20;
                                    x = rando.ints(20,(int)(paneWidth-iv.getImageView().getFitWidth())+1).findFirst().getAsInt();
                                    placeIt(iv,groupPage,x,y,pv);
                                    y=y+space+height;
                                }
                                else
                                {
                                    addTwoPanes();
                                    y = 20;
                                    x = rando.ints(20,(int)(paneWidth-iv.getImageView().getFitWidth())+1).findFirst().getAsInt();
                                    placeIt(iv,groupPage,x,y,pv);
                                    y=y+space+height;
                                }
                            }
                        }
                        else{
                            System.out.println("Hiba a képek lerakásánál!");
                        }



                    }
                }
                groupPage++;
                x=20;
                y=20;




            }
        }
        else{
            System.out.println("Nincsenek csoportok!");
        }
    }
    @SuppressWarnings("Duplicates")
    public void placeImagesToRandom(){
        if(!isAutomated){
        if(photoViews.size()!=0){
            int y=20;
            int x=20;


            boolean isgood = false;
            Random rando = new Random();
            for(int i = 0;i < photoViews.size();i++){

                ResizeableImageView iv = new ResizeableImageView(photoViews.get(i).getImage());
                iv.setPathOfImage(photoViews.get(i).getPathOfFile());
                iv.getImageView().setFitWidth(iv.getImageView().getImage().getWidth());
                iv.getImageView().setFitHeight(iv.getImageView().getImage().getHeight());
                int height = (int)iv.getImageView().getImage().getHeight();
                int width = (int) iv.getImageView().getImage().getWidth();

                int space = rando.nextInt(11)+10;
                if(iv.getImageView().getImage().getWidth() < paneWidth && iv.getImageView().getImage().getHeight() < paneHeight && !photoViews.get(i).isOnPane()){
                    x=rando.ints(20,(int)(paneWidth-iv.getImageView().getFitWidth())+1).findFirst().getAsInt();
                   if(paneHeight > (y+space+height) )
                   {
                       placeIt(iv,automatPaneNumber,x,y,photoViews.get(i));
                       y=y+space+height;

                   }
                    else
                    {
                       if(automatPaneNumber<listOfPanes.size()-1)
                       {
                       automatPaneNumber++;
                       y = 20;
                       x = rando.ints(20,(int)(paneWidth-iv.getImageView().getFitWidth())+1).findFirst().getAsInt();
                       placeIt(iv,automatPaneNumber,x,y,photoViews.get(i));
                       y=y+space+height;
                       //System.out.println("5.");


                        }
                        else
                            {
                                addTwoPanes();
                                automatPaneNumber++;

                                y = 20;
                                x = rando.ints(20,(int)(paneWidth-iv.getImageView().getFitWidth())+1).findFirst().getAsInt();
                                placeIt(iv,automatPaneNumber,x,y,photoViews.get(i));
                                y=y+space+height;
                         }
                   }
                }
                else{
                    System.out.println("Hiba a képek lerakásánál!");
                }



            }

        }

       automatPaneNumber++;
        }
        else
        {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Automata erendezés");
            alert.setHeaderText("Automata elrendezés használva!");
            alert.setContentText("Az automata elrendezést már használtad, innentől kézzel kell elrendezned. Adj hozzá több képet ha újra használni szeretnéd a funkciót.");

            alert.showAndWait();

        }
        isAutomated = true;
    }
    private void placeIt(ResizeableImageView iv,int numberOfPane, int x,int y,PhotoView pv){
        if(pv!=null){
            pv.setOnPane(true);
            pv.setOnPages(numberOfPane+1);
        }
        listOfPanes.get(numberOfPane).placeImageViewOnPane(iv);
        iv.setLayoutX(x);
        iv.setLayoutY(y);
    }

    public void saveAsImages(){
        Stage stage = new Stage();
        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle("Mappa kiválasztása");
        File selectedDir = directoryChooser.showDialog(stage);
        if(saveFileName==null){
            DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
            Date date = new Date();
            saveFileName=dateFormat.format(date);
        }
        if(selectedDir!=null){
            for(int i = 0; i < listOfPanes.size();i++) {
                listOfPanes.get(i).setZoomFactor(1d);
                BufferedImage bufImage = SwingFXUtils.fromFXImage(listOfPanes.get(i).snapshot(new SnapshotParameters(), null), null);
                String path = selectedDir.toURI().toString()+"/"+saveFileName+"_"+i+".png";
                File dir = new File(path.replace("file:/",""));
                try {
                    dir.createNewFile();
                    ImageIO.write(bufImage, "png", dir);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                listOfPanes.get(i).setZoomFactor(zoomFactor.doubleValue());
            }
        }
    }
    public void saveAsPDF(){
    Stage stage = new Stage();
    FileChooser fileChooser = new FileChooser();
    fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("pdf","*.pdf"));
    File pdfFile = fileChooser.showSaveDialog(stage);
    if (pdfFile != null) {

        try {
            ArrayList<com.itextpdf.text.Image> images = new ArrayList<>();
            FileOutputStream fos = null;
            com.itextpdf.text.Image image;

            for (int i = 0; i < listOfPanes.size(); i++) {
                listOfPanes.get(i).setZoomFactor(1d);
                BufferedImage bufImage = SwingFXUtils.fromFXImage(listOfPanes.get(i).snapshot(new SnapshotParameters(), null), null);
                FileOutputStream out = new FileOutputStream(new File("./temp.png"));
                ImageIO.write(bufImage, "png", out);
                out.flush();
                out.close();
                image = com.itextpdf.text.Image.getInstance("./temp.png");
                images.add(image);
                listOfPanes.get(i).setZoomFactor(zoomFactor.doubleValue());
            }
            Document doc = new Document(new Rectangle(paneWidth , paneHeight));
            fos = new FileOutputStream(pdfFile);
            PdfWriter.getInstance(doc, fos);
            doc.open();
            for (com.itextpdf.text.Image img : images) {
                img.setAbsolutePosition(0, 0);
                doc.add(img);
                doc.newPage();
                fos.flush();
            }
            doc.close();
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (Desktop.isDesktopSupported()) {
            Desktop desktop = Desktop.getDesktop();
            if (desktop.isSupported(Desktop.Action.OPEN)) {
                try {
                    desktop.open(pdfFile);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }





        //pb.setMaxSize(140,140);

        //pb.setMaxSize(140,140);

        //backgroundTask.run();

        //new Thread(backgroundTask).start();
        // bgThread.setDaemon(true);
        // bgThread.start();
        // backgroundTask.run();
        //removeElement(veil,save,true);
        //ProgressBar pb = new ProgressBar();
        /*ProgressIndicator pb =new ProgressIndicator();

        Label save = new Label();
        save.setFont(Font.font("Arial",28));
        save.setText("Mentés folyamatban...");

        Region veil = new Region();

        veil.setStyle("-fx-background-color: rgba(0,0,0,0.40)");
        veil.setPrefSize(400,400);

        stackPane.getChildren().addAll(veil,save);*/



        //pb.progressProperty().bind(bgsaveTask.progressProperty());
        //veil.visibleProperty().bind(bgsaveTask.runningProperty());
        //pb.visibleProperty().bind(bgsaveTask.runningProperty());

        //Thread th = new Thread(bgsaveTask);
        //th.setDaemon(true);
        //th.start();

        //removeElement(veil,save,false);





    }
    private void removeElement(Region r,Label p,boolean switcher){
        if(switcher)
        stackPane.getChildren().removeAll(r,p);
        else
        {
            r.setVisible(true);
            p.setVisible(true);
        }

    }


}
