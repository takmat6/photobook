package sample;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.StringConverter;

import java.beans.EventHandler;
import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class DataInit {
    @FXML
    ComboBox<PageSizes> comboBox = new ComboBox();
    @FXML
    Button saveButton = new Button();
    @FXML
    Button load;
    @FXML
    TextField widthText = new TextField();
    @FXML
    TextField heightText = new TextField();
    @FXML
    ListView<String> saveInstances=new ListView<>();
    @FXML
    SplitPane splitPaneDataInit = new SplitPane();

    @FXML
    CheckBox isOnSide = new CheckBox();
    @FXML
    CheckBox isWizardOn;
    public boolean wizardOn;
    private String saveFile=null;

    private String newFile=null;
    public String getNewFile() {
        return newFile;
    }
    public void setNewFile(String newFile) {
        this.newFile = newFile;
    }
    public String getSaveFile() {
        return saveFile;
    }
    private void setSaveFile(String saveFile){
        this.saveFile=saveFile;
    }
    private int width,height;
    public int getWidth() {
        return width;
    }
    public void setWidth(int width) {
        this.width = width;
    }
    public int getHeight() {
        return height;
    }
    public void setHeight(int height) {
        this.height = height;
    }

    public boolean isWizardOn() {
        return wizardOn;
    }

    @FXML
    private void initialize(){

        SplitPane.Divider divider = splitPaneDataInit.getDividers().get(0);
        divider.positionProperty().addListener((observable, oldvalue, newvalue) -> divider.setPosition(0.5));
        isOnSide.setDisable(true);
        isOnSide.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue && heightText.getText()!="") {
                changeXY(true);
            }
            else {
                changeXY(false);
            }
        });
        loadSaves();
        widthText.textProperty().addListener(((observable,oldV,newV) -> {
            if(!newV.equals("")){
                isOnSide.setDisable(false);
            }
            else
            {
                isOnSide.setDisable(true);
            }
        }));
        heightText.textProperty().addListener(((observable,oldV,newV) -> {
            if(!newV.equals("")){
                isOnSide.setDisable(false);
            }
            else
            {
                isOnSide.setDisable(true);
            }
        }));

        comboBox.getItems().addAll(PageSizes.values());
        comboBox.setOnAction(event -> {
           PageSizes pg = comboBox.getValue();
           widthText.setText(String.valueOf(pg.getX()));
           heightText.setText(String.valueOf(pg.getY()));
        });
    }
    private void changeXY(boolean b)
    {

            if(b  ){
                String temp = heightText.getText();
                heightText.setText(widthText.getText());
                widthText.setText(temp);
            }
            else{
                String temp = widthText.getText();
                widthText.setText(heightText.getText());
                heightText.setText(temp);
            }


    }
    public void loadSaves(){
        String path = getClass().getResource("saves").toString();
        //System.out.println(path);
        path = path.replace("file:/","");
        final File dir = new File(path);
        final String extension = "save";
        final FilenameFilter IMAGE_FILTER = (dir1, name) -> {

            return name.endsWith("." + extension);

        };
        ObservableList<String> names = FXCollections.observableArrayList();


        for (final File f : dir.listFiles(IMAGE_FILTER)) {
            names.add(f.getName());
            //System.out.println(f.getName());
        }
        saveInstances.setItems(names);
        saveInstances.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                setSaveFile(newValue);
            }
        });

    }
    public void loadProject(){
        if(saveFile!=null){
            setNewFile("load");
                   Stage stage = (Stage) load.getScene().getWindow();
                   stage.close();
        }
    }


    public void saveDataInit(){
        setNewFile("create");
        Stage stage = (Stage) saveButton.getScene().getWindow();
        setHeight(Integer.parseInt(heightText.getText()));
        setWidth(Integer.parseInt(widthText.getText()));
        if(isWizardOn.isSelected()){
            wizardOn=true;

        }
        else{
            wizardOn=false;
        }
        stage.close();
    }


}
