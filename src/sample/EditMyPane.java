package sample;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

import java.awt.*;
import java.io.File;
import java.io.FilenameFilter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class EditMyPane implements Pages {
    MyPane p;
    double orgSceneX, orgSceneY;
    double orgTranslateX, orgTranslateY;
    @FXML
    Label sliderLabel = new Label();
    @FXML
    HBox EditMenu ;
    boolean newText = false;
    @FXML
    Group center;
    @FXML
    Button saveProperties;
    @FXML
    BorderPane rootPane;

    @FXML
    GridPane TextEdit;
    @FXML
    Button editPage;
    @FXML
    Slider slider = new Slider();
    @FXML
    TextField labelTextField = new TextField();
    @FXML
    ColorPicker colorPicker= new ColorPicker(Color.BLACK);
    @FXML
    CheckBox borderCheck;
    @FXML
    VBox borderBox;
    @FXML
    Slider borderSlider;
    @FXML
    Slider borderMarginSlider;
    @FXML
    ColorPicker borderColorPicker;
    @FXML
    VBox borderMenu;
    @FXML
    HBox bottomSide;
    @FXML
    Button allPages;
    @FXML
    VBox Properties;
    @FXML
    ColorPicker bgColorPicker;
    @FXML
    Button backGroundImageZoom;
    @FXML
    Button addFloatingTextBox;
    @FXML
    Button addClipArt;
    @FXML
    VBox leftSide;
    private ArrayList<ResizeableImageView> layoutViews=new ArrayList<>();
    private ArrayList<String> labelIDs = new ArrayList<>();
    private ArrayList<FloatingText> floatingTexts = new ArrayList<>();
    private String lastLabelId="";
    Border pageBorder;
    private Color borderColor;
    private BorderWidths borderWidths;
    private Insets borderInsets;
    private ArrayList<ImageView> layoutList = new ArrayList<>();
    private ArrayList<Object> onPane = new ArrayList<>();
    private boolean backgroundEdit =false;
    private  ArrayList<String> fontsArray;

    private boolean textAdded=false;
    @FXML
    FlowPane layoutFlow;
    @FXML
    ScrollPane layoutScollPane;
    @FXML
    ScrollPane clipartScrollPane;
    @FXML
    FlowPane clipartFlow;
    @FXML
    ScrollPane textboxScrollPane;
    @FXML
    FlowPane textboxFlow;
    @FXML
    ScrollPane themeScrollPane;
    @FXML
    FlowPane themeFlow;
    @FXML
    Button addTheme;
    String fonts[];


    private Pane tempPane;


    double multiplier;
    @FXML
    private void initialize(){
        //
        borderMenu.getChildren().remove(borderBox);

        bgColorPicker.setOnAction(event -> {
            p.setBgColor(bgColorPicker.getValue());
        });
        borderCheck.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if(newValue==true){
                    //borderBox.visibleProperty().setValue(true);
                    borderMenu.getChildren().add(borderBox);
                    //p.setStyle("-fx-border-style: solid;"+"-fx-border-width: 1px;");
                    initBorder();
                    p.setBorder(pageBorder);
                    //p.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID,CornerRadii.EMPTY,BorderWidths.DEFAULT)));

                }

                else{

                    borderMenu.getChildren().remove(borderBox);
                    p.setBorder(null);//new Border(new BorderStroke(Color.TRANSPARENT,BorderStrokeStyle.NONE,CornerRadii.EMPTY,BorderWidths.DEFAULT))
                }
            }
        });

        TextEdit.visibleProperty().setValue(false);


        colorPicker.setStyle("-fx-color-label-visible: false ;");
        allPages.setOnAction(event -> {
            applyToAllPages();
        });

        backGroundImageZoom.setOnAction(event -> {
            tempPane=p.getMainPane();
            p.getBase().getChildren().remove(p.getMainPane());
            backgroundEdit=true;
            editBackgroundImage();
            Slider bgSlider = new Slider();
            bgSlider.setMin(1);
            bgSlider.setMax(100);
            bgSlider.setValue(p.backGroundImageView.getOpacity()*100);
            bgSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
                p.backGroundImageView.setOpacity(newValue.doubleValue()/100);

            });

            Properties.getChildren().clear();
            Properties.getChildren().add(bgSlider);
        });
        addFloatingTextBox.setOnAction(event -> {

            addTextBox();

        });
        addClipArt.setOnAction(event -> {
            ClipArt();
        });
        addTheme.setOnAction(event -> {
            addThemeToPage();
        });


    }
    private void fonts(FloatingTextBox floatingTextBox){
        if(fontsArray==null){
            fontsArray = new ArrayList<>();
            String fonts[] =

                    GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();

            for(String s : fonts){
                //if(!s.contains(" ")){
                    fontsArray.add(s);
                //}
            }
        }


        ComboBox<String> fontsForText = new ComboBox<>();
        TextField textBoxField = new TextField();
        TextField numberOfRows = new TextField();
        numberOnly(numberOfRows);
        Text test = new Text();
        textBoxField.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.ENTER) {
                floatingTextBox.setText(textBoxField.getText());
                test.setText(textBoxField.getText());
                Properties.getChildren().add(test);
                test.applyCss();
                double width = test.getLayoutBounds().getWidth()*multiplier;
                double height = test.getLayoutBounds().getHeight()*multiplier*1.1;
                System.out.println("width: " +width);
                Properties.getChildren().remove(test);
                if(floatingTextBox.getMaxTextWidth()<width){

                    int rowCount = 0;
                    if(numberOfRows.getText().trim().isEmpty()){
                        numberOfRows.setText(String.valueOf(3));
                    }

                        rowCount=Integer.parseInt(numberOfRows.getText());
                    System.out.println(rowCount);
                    floatingTextBox.setRowCount(rowCount);
                    double newMaxWidth = width*1.3/rowCount;
                    floatingTextBox.setMaxTextWidth((int)newMaxWidth);
                    //double multi = Math.ceil((width/(floatingTextBox.getMaxTextWidth()*0.9)));
                    //System.out.println(multi);
                    floatingTextBox.getImage().setPreserveRatio(false);
                    floatingTextBox.getImage().setFitWidth(floatingTextBox.getMaxTextWidth()*1.4);
                    floatingTextBox.getImage().setFitHeight((Integer.parseInt(numberOfRows.getText())*height*1.8));

                }
                else if(floatingTextBox.getMaxTextWidth()>width){
                    floatingTextBox.setMaxTextWidth((int)(width*1.2));
                }
            }
        });
        numberOfRows.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.ENTER) {
                floatingTextBox.setText(textBoxField.getText());
                test.setText(textBoxField.getText());
                Properties.getChildren().add(test);
                test.applyCss();
                double width = test.getLayoutBounds().getWidth()*multiplier;
                double height = test.getLayoutBounds().getHeight()*multiplier*1.1;
                System.out.println("width: " +width);
                Properties.getChildren().remove(test);
                if(floatingTextBox.getMaxTextWidth()<width){

                    int rowCount = 0;
                    if(numberOfRows.getText().trim().isEmpty()){
                        numberOfRows.setText(String.valueOf(3));
                    }

                    rowCount=Integer.parseInt(numberOfRows.getText());
                    System.out.println(rowCount);
                    double newMaxWidth = width*1.3/rowCount;
                    floatingTextBox.setMaxTextWidth((int)newMaxWidth);
                    //double multi = Math.ceil((width/(floatingTextBox.getMaxTextWidth()*0.9)));
                    //System.out.println(multi);
                    floatingTextBox.getImage().setPreserveRatio(false);
                    floatingTextBox.getImage().setFitWidth(floatingTextBox.getMaxTextWidth()*1.4);
                    floatingTextBox.getImage().setFitHeight((Integer.parseInt(numberOfRows.getText())*height*1.8));

                }
            }
        });
        fontsForText.setPromptText("Válassz betűtipust:");
        fontsForText.getItems().addAll(fontsArray);
        //pageProperties.getChildren().clear();

        TextField textBoxWidth = new TextField();
        TextField textBoxHeight = new TextField();
        numberOnly(textBoxHeight);
        numberOnly(textBoxWidth);
        textBoxWidth.textProperty().bind(floatingTextBox.getImage().fitWidthProperty().asString("%.0f"));
        textBoxHeight.textProperty().bind(floatingTextBox.getImage().fitHeightProperty().asString("%.0f"));


        Properties.getChildren().clear();
        Properties.getChildren().addAll(textBoxField,fontsForText,new Label("Szöveg sorainak száma:"),numberOfRows,new Label("Szövegdoboz szélessége::"),textBoxWidth,new Label("Szövegdoboz magassága:"),textBoxHeight);
        fontsForText.valueProperty().addListener((observable, oldValue, newValue) -> {


            floatingTextBox.setTextFont(newValue);
           /* if(floatingTextBox.isEditable){

            }*/
        });

    }

    private void addThemeToPage() {
        if(backgroundEdit){
            p.getBase().getChildren().add(tempPane);
        }
        if(!bottomSide.getChildren().contains(themeScrollPane)) {
            bottomSide.getChildren().clear();
            themeScrollPane = (ScrollPane) loadUI("themesui");
            bottomSide.getChildren().clear();
            bottomSide.getChildren().add(themeScrollPane);
            themeFlow = (FlowPane) themeScrollPane.getContent();


            Button addThemeToAllPages = new Button("Téma beállítása az össze soldalra");
            addThemeToAllPages.setOnAction(event -> {
                String theme = p.getTheme();
                for(ThemeDisplay td : listOfThemes){
                    if(td.getThemeName().equals(theme)){
                        for(MyPane myPane : listOfPanes){
                            myPane.setTheme(theme);
                            System.out.println(listOfPanes.indexOf(myPane)+" "+ myPane.getTheme());
                            if(listOfPanes.indexOf(myPane) ==0){
                                myPane.addBackGroundImageWithoutZoom(td.getFront());

                            }
                            else if(listOfPanes.indexOf(myPane) ==listOfPanes.size()-1){
                                myPane.addBackGroundImageWithoutZoom(td.getEnd());
                            }
                            else{
                                myPane.addBackGroundImageWithoutZoom(td.getMain());
                            }

                        }
                    }
                }

            });
            Properties.getChildren().add(addThemeToAllPages);




            if(listOfThemes.size()>0){

                if(p.getPrefWidth()<p.getPrefHeight()){
                    for(ThemeDisplay td : listOfThemes){
                        if(!td.isRotated())
                    themeFlow.getChildren().add(td);
                       td.setBackGround(p);
                    }



                }
                else{
                    for(ThemeDisplay td : listOfThemes) {
                        if (td.isRotated())
                            themeFlow.getChildren().add(td);
                        td.setBackGround(p);
                    }

                }


            }
            else {
                String path = getClass().getResource("themes").toString();
                //System.out.println(path);
                path = path.replace("file:/", "");
                final File dir = new File(path);
                final String[] EXTENSIONS = new String[]{"jpeg", "jpg", "png", "bmp"};
                final FilenameFilter IMAGE_FILTER = (dir1, name) -> {
                    for (final String ext : EXTENSIONS) {
                        if (name.endsWith("." + ext)) {
                            return (true);
                        }
                    }
                    return (false);
                };

                if (dir.isDirectory()) {

                    List<File> images = Arrays.asList(dir.listFiles(IMAGE_FILTER));
                   /* for (final File f : dir.listFiles(IMAGE_FILTER)) {
                        String pathOfImage = f.toURI().toString();
                        Image img = new Image(pathOfImage);
                        System.out.println(f.toString());

                        ImageView iv = new ImageView(img);
                        iv.setFitWidth(150);
                        iv.setFitHeight(150);
                        iv.setPreserveRatio(true);

                        themeFlow.getChildren().add(iv);
                        iv.setOnMouseClicked(event -> {
                            p.addBackGroundImageWithoutZoom(iv.getImage());
                        });



                    }*/
                    for (int i = 0; i < images.size(); i = i + 6) {
                        Image end = null;
                        Image front = null;
                        Image main = null;
                        Image endRotated = null;
                        Image frontRotated = null;
                        Image mainRotated = null;

                        if (images.get(i + 1).toURI().toString().contains("End") && images.get(i + 1).toURI().toString().contains("Rotated")) {
                            endRotated = new Image(images.get(i + 1).toURI().toString());
                        }
                        if (images.get(i + 3).toURI().toString().contains("Front") && images.get(i + 3).toURI().toString().contains("Rotated")) {
                            frontRotated = new Image(images.get(i + 3).toURI().toString());
                        }
                        if (images.get(i + 5).toURI().toString().contains("Main") && images.get(i + 5).toURI().toString().contains("Rotated")) {
                            mainRotated = new Image(images.get(i + 5).toURI().toString());
                        }

                        if (images.get(i).toURI().toString().contains("End")) {

                            end = new Image(images.get(i).toURI().toString());
                        }
                        if (images.get(i + 2).toURI().toString().contains("Front")) {
                            front = new Image(images.get(i + 2).toURI().toString());
                        }
                        if (images.get(i + 4).toURI().toString().contains("Main")) {
                            main = new Image(images.get(i + 4).toURI().toString());
                        }

                        /*
                        System.out.println("0." + images.get(i).toURI().toString());
                        System.out.println("1." + images.get(i + 1).toURI().toString());
                        System.out.println("2." + images.get(i + 2).toURI().toString());
                        System.out.println("3." + images.get(i + 3).toURI().toString());
                        System.out.println("4." + images.get(i + 4).toURI().toString());
                        System.out.println("5." + images.get(i + 5).toURI().toString());
                        */

                        ThemeDisplay themeDisplay = new ThemeDisplay(front, main, end);
                        themeDisplay.setPadding(new Insets(0, 10, 0, 10));
                        themeDisplay.setRotated(false);
                        listOfThemes.add(themeDisplay);

                        ThemeDisplay themeDisplayRotated = new ThemeDisplay(frontRotated, mainRotated, endRotated);
                        themeDisplayRotated.setPadding(new Insets(0, 10, 0, 10));
                        themeDisplayRotated.setRotated(true);

                        String temp =(images.get(i).toURI().toString().replace("End", "").replace("file:",""));
                        String[] temp2 = temp.split("/");

                        themeDisplay.setThemeName(temp2[temp2.length-1].replace(".png",""));
                        System.out.println(themeDisplay.getThemeName());
                        listOfThemes.add(themeDisplayRotated);
                        temp =(images.get(i+1).toURI().toString().replace("End", "").replace("file:",""));
                        temp2 = temp.split("/");
                        themeDisplayRotated.setThemeName(temp2[temp2.length-1].replace(".png",""));
                        //System.out.println(themeDisplay.getThemeName());
                        System.out.println(themeDisplayRotated.getThemeName());
                        if (p.getPaneWidth() > p.getPaneHeight()) {
                            themeFlow.getChildren().add(themeDisplayRotated);
                            themeDisplayRotated.setOnMouseClicked(event -> {
                                p.setTheme(themeDisplayRotated.getThemeName());
                                if (listOfPanes.indexOf(p) == 0){
                                    p.addBackGroundImageWithoutZoom(themeDisplayRotated.getFront());

                                }
                                else if (listOfPanes.indexOf(p) == listOfPanes.size() - 1){
                                    p.addBackGroundImageWithoutZoom(themeDisplayRotated.getEnd());

                                }

                                else
                                {
                                    p.addBackGroundImageWithoutZoom(themeDisplayRotated.getMain());

                                }
                                p.setBgColor(null);


                            });
                        } else {
                            themeFlow.getChildren().add(themeDisplay);
                            themeDisplay.setOnMouseClicked(event -> {
                                p.setTheme(themeDisplay.getThemeName());
                                if (listOfPanes.indexOf(p) == 0){
                                    p.addBackGroundImageWithoutZoom(themeDisplay.getFront());
                                    p.setBgColor(null);
                                }
                                else if (listOfPanes.indexOf(p) == listOfPanes.size() - 1){
                                    p.addBackGroundImageWithoutZoom(themeDisplay.getEnd());
                                    p.setBgColor(null);
                                }

                                else
                                {
                                    p.addBackGroundImageWithoutZoom(themeDisplay.getMain());
                                    p.setBgColor(null);
                                }

                            });
                        }


                    }
                    //themeFlow.setOrientation(Orientation.HORIZONTAL);
                    //scrollPaneFrames.setContent(flowFrames);
                    //scrollPaneFrames.setFitToWidth(true);
                    //scrollPaneFrames.setFitToHeight(true);
                } else {
                    System.out.println("nem jó a megadott útvonal");
                }

            }

            themeFlow.setPrefSize(500*(listOfThemes.size()/2),0.0);
        }
    }

    private void editBackgroundImage() {

    }



    private void applyToAllPages(){
        for(MyPane pane : listOfPanes){
            pane.getBackGroundPane().setBorder(pageBorder);
            pane.setBorderColor(borderColor);
        }
    }
    public void layoutPlans(){
        if(backgroundEdit){
            p.getBase().getChildren().add(tempPane);
        }
        if(!bottomSide.getChildren().contains(layoutScollPane)) {
            bottomSide.getChildren().clear();
            layoutScollPane = (ScrollPane) loadUI("layout");
            bottomSide.getChildren().clear();
            bottomSide.getChildren().add(layoutScollPane);
            layoutFlow = (FlowPane) layoutScollPane.getContent();



            if (layoutList.isEmpty()) {

                String path = getClass().getResource("layouts").toString();
                //System.out.println(path);
                path = path.replace("file:/", "");
                final File dir = new File(path);
                final String[] EXTENSIONS = new String[]{"jpeg", "jpg", "png", "bmp"};
                final FilenameFilter IMAGE_FILTER = (dir1, name) -> {
                    for (final String ext : EXTENSIONS) {
                        if (name.endsWith("." + ext)) {
                            return (true);
                        }
                    }
                    return (false);
                };

                if (dir.isDirectory()) {
                    for (final File f : dir.listFiles(IMAGE_FILTER)) {
                        Image img = new Image(f.toURI().toString());
                        System.out.println(f.toString());

                        ImageView iv = new ImageView(img);
                        iv.setFitWidth(150);
                        iv.setFitHeight(150);
                        iv.setPreserveRatio(true);
                        layoutList.add(iv);
                        layoutFlow.getChildren().add(iv);
                        for(int i = 0;i < 4;i++){
                            layoutViews.add(createDummyView(p.getPaneWidth()/2-50,(p.getPaneHeight()/3)+20));
                        }
                        String pic = "";
                        iv.setOnMouseClicked(event -> {
                            p.getMainPane().getChildren().clear();
                            if(f.toString().contains("vertical1")){

                                placeItOnPane(layoutViews.get(0),p.getPaneWidth()/2+20,30);
                                placeItOnPane(layoutViews.get(1),30,(p.getPaneHeight()/2)-(int)layoutViews.get(1).getPrefHeight()/2);
                                placeItOnPane(layoutViews.get(2),p.getPaneWidth()/2+20,(p.getPaneHeight())-((int)layoutViews.get(2).getPrefHeight()+20));
                            }
                            else if(f.toString().contains("vertical2"))
                            {

                            }
                            else if(f.toString().contains("vertical3"))
                            {

                            }
                            else if(f.toString().contains("vertical4"))
                            {

                            }

                        });


                    }
                    layoutFlow.setOrientation(Orientation.HORIZONTAL);
                    //scrollPaneFrames.setContent(flowFrames);
                    //scrollPaneFrames.setFitToWidth(true);
                    //scrollPaneFrames.setFitToHeight(true);
                } else {
                    System.out.println("nem jó a megadott útvonal");
                }
            }

        }

    }
    private void ClipArt(){
        if(backgroundEdit){
            p.getBase().getChildren().add(tempPane);
        }
        if(!bottomSide.getChildren().contains(clipartScrollPane)) {
            bottomSide.getChildren().clear();
            clipartScrollPane = (ScrollPane) loadUI("clipartui");
            bottomSide.getChildren().clear();
            bottomSide.getChildren().add(clipartScrollPane);
            clipartFlow = (FlowPane) clipartScrollPane.getContent();





                String path = getClass().getResource("cliparts").toString();
                System.out.println(path);
                path = path.replace("file:/", "");
                final File dir = new File(path);
                final String[] EXTENSIONS = new String[]{"jpeg", "jpg", "png", "bmp","PNG"};
                final FilenameFilter IMAGE_FILTER = (dir1, name) -> {
                    for (final String ext : EXTENSIONS) {
                        if (name.endsWith("." + ext)) {
                            return (true);
                        }
                    }
                    return (false);
                };

                if (dir.isDirectory()) {
                    for (final File f : dir.listFiles(IMAGE_FILTER)) {
                        Image img = new Image(f.toURI().toString());
                        System.out.println(f.toString());

                        ImageView iv = new ImageView(img);
                        iv.setFitWidth(150);
                        iv.setFitHeight(150);
                        iv.setPreserveRatio(true);
                        clipartFlow.getChildren().add(iv);
                        iv.setOnDragDetected(event -> {
                            Dragboard db = iv.startDragAndDrop(TransferMode.ANY);
                            ClipboardContent content = new ClipboardContent();
                            content.putString(f.toURI().toString());
                            content.putImage(iv.getImage());
                            db.setContent(content);
                            event.consume();
                        });
                        iv.setOnDragDone(event -> {
                            Dragboard db = event.getDragboard();
                            if(event.getTransferMode()==TransferMode.MOVE)

                                event.consume();

                        });



                    }
                    clipartFlow.setOrientation(Orientation.HORIZONTAL);
                    //scrollPaneFrames.setContent(flowFrames);
                    //scrollPaneFrames.setFitToWidth(true);
                    //scrollPaneFrames.setFitToHeight(true);
                } else {
                    System.out.println("nem jó a megadott útvonal");
                }


        }


    }
    private void placeItOnPane(ResizeableImageView iv,int x,int y){
        p.placeImageViewOnPane(iv);
        iv.setTranslateX(x);
        iv.setTranslateY(y);

    }

    private ResizeableImageView createDummyView(int width,int height){
        double bw=5.0 ;
        Color bc = Color.BLACK;
        ResizeableImageView iv = new ResizeableImageView();
        iv.setBGColor(Color.LIGHTGREY);
        iv.setPrefWidth(width);
        iv.setPrefHeight(height);
        iv.setBorderWidth(bw);
        iv.setBorderColor(bc);
        Border brdr = new Border(new BorderStroke(bc, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(bw,bw,bw,bw), new Insets(0, 0, 0, 0)));
        iv.setBorder(brdr);
        return iv;
    }
    private Parent loadUI(String ui){
        Parent root = null;
        FXMLLoader loader = new FXMLLoader(getClass().getResource(ui+".fxml"));
        try {
            root = loader.load();
        }
        catch (Exception e){

        }



        return root;
    }
    private void initBorder(){
        //borderColorPicker = new ColorPicker(Color.BLACK);

        borderColorPicker.setValue(Color.BLACK);
        borderColorPicker.valueProperty().addListener(((observable, oldValue, newValue) -> {
            System.out.println(newValue.toString());
            borderColor=newValue;
            pageBorder=new Border(new BorderStroke(newValue,BorderStrokeStyle.SOLID,CornerRadii.EMPTY,borderWidths,borderInsets));
            p.getBackGroundPane().setBorder(pageBorder);
            p.setBorderColor(newValue);

        }));
        borderSlider.valueProperty().addListener(((observable, oldValue, newValue) -> {
            System.out.println(newValue.doubleValue());

            borderWidths=new BorderWidths(newValue.doubleValue(),newValue.doubleValue(),newValue.doubleValue(),newValue.doubleValue());
            pageBorder = new Border(new BorderStroke(borderColor,BorderStrokeStyle.SOLID,CornerRadii.EMPTY,borderWidths,borderInsets));
            p.setBorderWidth(newValue.doubleValue());
            p.getBackGroundPane().setBorder(pageBorder);

        }));
        borderMarginSlider.valueProperty().addListener(((observable, oldValue, newValue) -> {
            borderInsets=new Insets(newValue.doubleValue(),newValue.doubleValue(),newValue.doubleValue(),newValue.doubleValue());
            pageBorder = new Border(new BorderStroke(borderColor,BorderStrokeStyle.SOLID,CornerRadii.EMPTY,borderWidths,borderInsets));
            p.setBorderInsets(newValue.doubleValue());
            p.getBackGroundPane().setBorder(pageBorder);

        }));

        //pageBorder = new Border(new BorderStroke(borderColor,BorderStrokeStyle.SOLID,CornerRadii.EMPTY,borderWidths,borderInsets));

    }
    public void initData(MyPane myPane){
        p = myPane;

        if(p.getBorderWidth()>0.0){
            borderCheck.setSelected(true);
            borderSlider.setValue(p.getBorderWidth());
            borderMarginSlider.setValue(p.getBackGroundPane().getBorder().getInsets().getBottom());
            borderColorPicker.setValue(p.getBorderColor());
        }
        multiplier=1/p.getZoomFactor();
        //System.out.println(multiplier);
        //center.setPrefSize(p.getWidth(),p.getHeight());
        center.getChildren().add(p);

    }
    public void addText()
    {
        if(backgroundEdit){
            p.getBase().getChildren().add(tempPane);
        }
        //System.out.println(newText);

        //newText=true;

            p.MainPane.setOnMouseClicked(event -> {
                if (event.getTarget() == p.MainPane) {
                    FloatingText label = new FloatingText("Szöveg");
                    floatingTexts.add(label);
                    label.createRandomID();
                    while (labelIDs.contains(label.getIdentity())) {
                        label.createRandomID();
                    }
                    labelIDs.add(label.getIdentity());
                    lastLabelId = label.getIdentity();

                    label.setMultiplier(multiplier);

                    //label.setStyle("-fx-font-size: " + (multiplier * 24) + ";");
                    label.setFont(Font.font(label.getFont().getName(), (int) (multiplier * 24)));

                    System.out.println(label.getFont().getSize());
                    p.placeFloatingTextOnPane(label, (int) event.getX(), (int) event.getY());



                    /*label.setOnMouseClicked(event1 -> {
                        if(event1.getClickCount()==2){
                            editText(label);
                        }
                        //lastLabelId = label.getIdentity();

                    });*/
                    slider.setValue(24);

                    editText(label);


                } else if (event.getTarget().getClass() == ResizeableImageView.class) {
                    FloatingText label = new FloatingText("Szöveg");
                    floatingTexts.add(label);
                    label.createRandomID();
                    while (labelIDs.contains(label.getIdentity())) {
                        label.createRandomID();
                    }
                    labelIDs.add(label.getIdentity());
                    lastLabelId = label.getIdentity();

                    label.setMultiplier(multiplier);

                    //label.setStyle("-fx-font-size: " + (multiplier * 24) + ";");
                    label.setFont(Font.font(label.getFont().getName(), (int) (multiplier * 24)));

                    System.out.println(label.getFont().getSize());
                    p.placeFloatingTextOnPane(label, (int) event.getX(), (int) event.getY());



                    /*label.setOnMouseClicked(event1 -> {
                        if(event1.getClickCount()==2){
                            editText(label);
                        }
                        //lastLabelId = label.getIdentity();

                    });*/
                    slider.setValue(24);

                    editText(label);

                }


                p.MainPane.setOnMouseClicked(event1 -> {
                });
            });



    }

    public void addTextBox()
    {
        if(backgroundEdit){
            p.getBase().getChildren().add(tempPane);
        }
        //System.out.println(newText);

        //newText=true;
            p.getMainPane().setOnMouseClicked(event -> {
                if (event.getTarget() == p.MainPane) {
                    String path = getClass().getResource("textbox/bubbleRight.png").toString();
                    Image image = new Image(path);
                    //floatingTextBox.setImage(image);
                    String string = "Ide kerül majd a szöveg.";

                    FloatingTextBox floatingTextBox = new FloatingTextBox(image, string, p.getPaneWidth() / 3, multiplier);
                    //addTextBox(floatingTextBox);
                    floatingTextBox.setBgImagePath(path);
                    floatingTextBox.isEditable = true;
                    showTextBoxImages(floatingTextBox);
                    if (listOfTextBoxImages.size() > 0) {
                        for (TextBoxImage iv : listOfTextBoxImages) {
                            iv.setOnMouseClicked(e -> {
                                floatingTextBox.setImage(iv.getImage());
                                //floatingTextBox.setTextFont("Arial");
                                floatingTextBox.setBgImagePath(iv.getImagePath());
                            });
                        }
                    }
                    fonts(floatingTextBox);
                    System.out.println("Image w: " + image.getWidth() + " h " + image.getHeight());


                    //label.setMultiplier(multiplier);

                    //floatingTextBox.setStyle("-fx-font-size: " + (multiplier * 14) + ";");//multiplier
                    floatingTextBox.setFontAndSize(floatingTextBox.getFont().getName(), (int) (multiplier * 14));
                    //floatingTextBox.setFontSize((int)multiplier*24);

                    //System.out.println(label.getFont().getSize());
                    p.placeFloatingTextBoxOnPane(floatingTextBox, (int) event.getX(), (int) event.getY());



                        /*label.setOnMouseClicked(event1 -> {
                            if(event1.getClickCount()==2){
                                editText(label);
                            }
                            //lastLabelId = label.getIdentity();

                        });*/
                    //slider.setValue(24);

                    //editText(label);


                }

                p.MainPane.setOnMouseClicked(event1 -> {});
            });

    }

    private void showTextBoxImages(FloatingTextBox floatingTextBox) {

        if(!bottomSide.getChildren().contains(textboxScrollPane)) {
            bottomSide.getChildren().clear();
            textboxScrollPane = (ScrollPane) loadUI("textboxBGs");
            bottomSide.getChildren().clear();
            bottomSide.getChildren().add(textboxScrollPane);
            textboxFlow = (FlowPane) textboxScrollPane.getContent();





            String path = getClass().getResource("textbox").toString();
            System.out.println(path);
            path = path.replace("file:/", "");
            final File dir = new File(path);
            final String[] EXTENSIONS = new String[]{"jpeg", "jpg", "png", "bmp","PNG"};
            final FilenameFilter IMAGE_FILTER = (dir1, name) -> {
                for (final String ext : EXTENSIONS) {
                    if (name.endsWith("." + ext)) {
                        return (true);
                    }
                }
                return (false);
            };

            if (dir.isDirectory()) {
                for (final File f : dir.listFiles(IMAGE_FILTER)) {
                    Image img = new Image(f.toURI().toString());
                    System.out.println(f.toString());

                    TextBoxImage iv = new TextBoxImage(img);
                    iv.setFitWidth(150);
                    iv.setFitHeight(150);
                    iv.setPreserveRatio(true);
                    iv.setImagePath(f.getPath());
                    textboxFlow.getChildren().add(iv);
                    listOfTextBoxImages.add(iv);
                    iv.setOnMouseClicked(event -> {
                        floatingTextBox.setImage(iv.getImage());
                        //floatingTextBox.setTextFont("Arial");
                        floatingTextBox.setBgImagePath(f.getPath());
                    });



                }
                textboxFlow.setOrientation(Orientation.HORIZONTAL);
                //scrollPaneFrames.setContent(flowFrames);
                //scrollPaneFrames.setFitToWidth(true);
                //scrollPaneFrames.setFitToHeight(true);
            } else {
                System.out.println("nem jó a megadott útvonal");
            }


        }

    }


    public void editText(FloatingText ft){

        bottomSide.getChildren().clear();
        bottomSide.getChildren().add(TextEdit);



        TextEdit.visibleProperty().setValue(true);

        ComboBox<String> textFonts = new ComboBox<>();
        if(fontsArray==null)
        {
            fontsArray = new ArrayList<>();
            String fonts[] =

                    GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();

            for(String s : fonts){
                //if(!s.contains(" ")){
                fontsArray.add(s);
                //}
            }
        }
        textFonts.getItems().addAll(fontsArray);
        TextEdit.add(textFonts,1,0);

        textFonts.valueProperty().addListener((observable, oldValue, newValue) -> ft.setFont(Font.font(newValue,ft.getFontSize())));

        Properties.getChildren().clear();
        labelTextField.setText("");
        ft.setEditable(true);
        labelTextField.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if(event.getCode().equals(KeyCode.ENTER)){
                    ft.setText(labelTextField.getText());
                }
            }
        });
        slider.setValue((int)(ft.getFont().getSize()*multiplier));
        sliderLabel.setText(String.valueOf(24));


        slider.valueProperty().addListener((observable, oldValue, newValue) -> {

            //ft.setStyle("-fx-font-size: "+(int)(multiplier*newValue.doubleValue())+";");
            //label.setFont(new Font(newValue.doubleValue()));
            ft.setFontSize(newValue.intValue());
            //ft.setFont(Font.font(ft.getFont().getName(),newValue.intValue()));
            slider.setValue(newValue.doubleValue());
            //System.out.println(label.getFont().getSize());
            sliderLabel.setText(String.valueOf(newValue.intValue()));
        });

        colorPicker.valueProperty().addListener((observable, oldValue, newValue) -> {

            ft.setFontColor(newValue);
        });


        saveProperties.setOnAction(event -> {

            ft.setEditable(false);
            TextEdit.visibleProperty().setValue(false);

        });

    }

    private void numberOnly(TextField tf){
        tf.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d*")) {
                tf.setText(newValue.replaceAll("[^\\d]", ""));
            }
        });
    }





}
