package sample;


import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

import java.util.Random;

public class FloatingText extends Text {
    private int fontSize=24;
    private double multiplier=1;
    private String identity;
    private Boolean editable=true;
    private String fontOfText;

    public Boolean getEditable() {
        return editable;
    }

    public void setEditable(Boolean editable) {
        this.editable = editable;
    }



    public int getFontSize() {
        return fontSize;
    }

    public void setMultiplier(double multiplier) {
        this.multiplier = multiplier;
    }

    public FloatingText() {
    }

    public FloatingText(String text) {
        super(text);
        //addDragAndDrop();

    }
    public FloatingText(String text, int size){
        super(text);

    }
    public FloatingText(String text,int fontSize,Color fontColor){
        super(text);
        setFontSize(fontSize);
        setFontColor(fontColor);
        fontOfText="Arial";
    }
    public void setFont(String font){
        fontOfText=font;
        this.setFont(Font.font(font,fontSize));
    }



    public String getIdentity() {
        return identity;
    }

    public void setIdentity(String identity) {
        this.identity = identity;
    }

    public void setFontSize(int size){
        if(editable) {
            this.setFont(Font.font(this.getFont().getName(),size*multiplier));
            //this.setFont(new Font((int) multiplier * size));
            //this.setStyle("-fx-font-size: " + (multiplier * size) + ";");
            fontSize = (int) multiplier * size;
        }
    }
    public void createRandomID(){
        Random rando = new Random();
        this.setIdentity(String.valueOf(rando.nextInt(10000)));
    }
    public void setFontColor(Color color){

        if(editable) {
            //this.setTextFill(color);
            this.setFill(color);
        }
    }
}
