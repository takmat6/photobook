package sample;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Font;
import javafx.scene.text.Text;


public class FloatingTextBox extends StackPane {

    @FXML
    ImageView image;
    @FXML
    Text text;
    @FXML
    StackPane FloatingTextBoxBase;
    private int fontSize;
    private int maxTextWidth;
    private String bgImagePath;

    private int rowCount;
    public String getBgImagePath() {
        return bgImagePath;
    }

    public void setBgImagePath(String bgImagePath) {
        this.bgImagePath = bgImagePath;
    }

    public int getMaxTextWidth() {
        return maxTextWidth;
    }

    public void setMaxTextWidth(int maxTextWidth) {
        this.maxTextWidth = maxTextWidth;
        text.maxWidth(maxTextWidth);
        text.setWrappingWidth(maxTextWidth);
    }

    public void setFontAndSize(String font, int fontSize){
        text.setFont(Font.font(font,fontSize));
    }
    public int getFontSize() {
        return fontSize;
    }

    public void setFontSize(int fontSize) {
        this.fontSize = fontSize;
    }

    private double multiplier;
    public boolean isEditable=false;


    public FloatingTextBox() {
        loadUI();
        //image.fitHeightProperty().bind(text.);


    }
    public void setTextFont(String font){
        System.out.println(fontSize);
        text.setFont(Font.font(font,text.getFont().getSize()));
        //this.setStyle("-fx-font: "+fontSize+" "+ font+";");
    }

    public ImageView getImage() {
        return image;
    }

    public FloatingTextBox(Image img, String txt, int maxwidth, double multiplier) {
        loadUI();
        image.setImage(img);
        setMaxTextWidth(maxwidth);


        image.setPreserveRatio(true);
        text.applyCss();
        double width = text.getLayoutBounds().getWidth()*1.05*multiplier;
        double height = text.getLayoutBounds().getHeight()*1.05*multiplier;
        this.multiplier=multiplier;
        System.out.println(width+" "+height);
        System.out.println("textheight "+text.getLayoutBounds().getHeight());
        text.setText(txt);
        text.maxWidth(maxwidth);
        text.setWrappingWidth(maxwidth);
        System.out.println("textwidth "+text.getLayoutBounds().getWidth());
        image.setFitWidth(maxwidth*1.2);
        System.out.println("imagefitheight "+image.getFitHeight());

        System.out.println(image.getFitWidth());
        /*int h = (int)image.getFitHeight()/2;
        int w = (int)image.getFitWidth()/2;
        text.setTranslateY((h-(multiplier*25))-height/2);
        text.setTranslateX(w-width/2);*/




    }
    public Font getFont(){
        return text.getFont();
    }
    public void setImage(Image img){
        image.setImage(img);
    }
    public void setText(String txt){
        text.setText(txt);
    }

    public String getText(){
        return text.getText();
    }
    private void loadUI(){
        FXMLLoader loader = new FXMLLoader(getClass().getResource("floatingtextbox.fxml"));
        loader.setRoot(this);
        loader.setController(this);
        try {
            loader.load();
        } catch (Exception e) {

        }

    }

    public void setRowCount(int rowCount) {
        this.rowCount=rowCount;
    }
    public int getRowCount(){
        return this.rowCount;
    }
}
