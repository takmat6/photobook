package sample;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Bounds;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.geometry.Rectangle2D;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import java.io.IOException;

//import static sample.PhotoViews.photoViews;

public class MyPane extends StackPane implements ResizeableImageViews,Pages, PhotoViews{




    double orgSceneX, orgSceneY;
    double orgTranslateX, orgTranslateY;
    private DoubleProperty zoomFactor = new SimpleDoubleProperty(0.5d);
    private int maxWidth;
    private int maxHeight;
    @FXML
    Pane BackGroundPane;
    @FXML
    Pane MainPane;
    @FXML
    StackPane Base;


    public StackPane getBase() {
        return Base;
    }

    public ImageView getBackGroundImageView() {
        return backGroundImageView;
    }

    private int MIN_PIXELS=10;
    private int width , height;

    private MyPaneContextMenu paneContextMenu = new MyPaneContextMenu(this);

    private Color borderColor=null;
    private double borderWidth=0.0;
    private double borderInsets=0.0;
    private Color imageBorderColor=null;
    private BorderWidths imageBorderWidths=null;
    private Color bgColor=null;
    private ShapeCreator shapeCreator;
    private String theme;

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    ImageView backGroundImageView;

    private Boolean inEditMode = false;
    public int getPaneWidth() {
        return maxWidth;
    }


    public void clearBackground(){
        this.backGroundImageView.setImage(null);
    }

    public int getPaneHeight() {
        return maxHeight;
    }

    public double getBorderInsets() {
        return borderInsets;
    }

    public void setBorderInsets(double borderInsets) {
        this.borderInsets = borderInsets;
    }

    public Color getBgColor() {
        return bgColor;
    }

    public Pane getBackGroundPane() {
        return BackGroundPane;
    }

    public Pane getMainPane() {
        return MainPane;
    }

    public void setBgColor(Color bgColor) {
        if(bgColor!=null){
            this.bgColor = bgColor;
            BackGroundPane.setStyle("-fx-background-color:"+bgColor.toString().replace("0x","#")+";");
        }
        else{
            this.bgColor=null;
        }

    }

    public Boolean getInEditMode() {
        return inEditMode;
    }

    public void setInEditMode(Boolean inEditMode) {
        this.inEditMode = inEditMode;
    }

    public double getBorderWidth() {
        return borderWidth;
    }

    public void setBorderWidth(double borderWidth) {
        this.borderWidth = borderWidth;
    }

    public Color getBorderColor() {
        return borderColor;
    }

    public void setBorderColor(Color borderColor) {
        this.borderColor = borderColor;
    }

    public void setZoomFactor(double zoomFactor) {
        this.zoomFactor.set(zoomFactor);
    }


    public MyPane(int width,int height) {
        super();
        maxWidth=width;
        maxHeight=height;
        loadUI();
        init();
        shapeCreator=new ShapeCreator();
        //loadUI();



    }
    private void loadUI(){
        FXMLLoader loader = new FXMLLoader(getClass().getResource("mypane.fxml"));
        loader.setRoot(this);
        loader.setController(this);
        try {
            loader.load();
        } catch (Exception e) {

        }



    }
    private void init(){
        this.scaleXProperty().bind(zoomFactor);
        this.scaleYProperty().bind(zoomFactor);

        MainPane.setOnDragOver(event -> {
            if (event.getDragboard().hasImage()) {
                event.acceptTransferModes(TransferMode.MOVE);
            }
            event.consume();
        });
        MainPane.setOnDragDropped(event -> {
            Dragboard db = event.getDragboard();
            if (db.hasImage()) {
                ResizeableImageView iv = new ResizeableImageView(db.getImage());
                String path =db.getString();
                System.out.println(path);
                if(path.contains("clipart")){
                    iv.setImageView(false);
                }
                if(path.contains("file:/")){
                    path=path.replace("file:/","");
                }
                iv.setPathOfImage(path);
                iv.getImageView().setPreserveRatio(true);
                for(PhotoView pv : photoViews){
                    if(pv.getPathOfFile().equals(path)){
                        pv.setOnPages((listOfPanes.indexOf(this)+1));
                    }
                }
                iv.getImageView().setFitWidth(db.getImage().getWidth() > maxWidth ? maxWidth-50 : db.getImage().getWidth());
                iv.getImageView().setFitHeight(db.getImage().getHeight() > maxHeight ? maxHeight-50 : db.getImage().getHeight());
                addContextMenu(iv);
                imageViewAddDragAndDrop(iv);
                MainPane.getChildren().add(iv);
            }
            event.setDropCompleted(true);
            event.consume();
        });

        MainPane.setOnContextMenuRequested(event -> {
            if(event.getTarget() instanceof Pane){

                paneContextMenu.show(this,event.getScreenX(),event.getScreenY());
            }
        });
        MainPane.setOnMouseClicked(event -> {
            if(paneContextMenu.isShowing()){
                paneContextMenu.hide();
            }

        });
    }
    public MyPane getPane(){
        return this;
    }

    private void addDragAndDropFloatingText(FloatingText ft){
        ft.setOnMousePressed(event -> {

            orgSceneX = event.getSceneX()*(1.0/zoomFactor.doubleValue());//
            orgSceneY = event.getSceneY()*(1.0/zoomFactor.doubleValue());//*multiplier

            orgTranslateX = ((FloatingText)(event.getSource())).getTranslateX();
            orgTranslateY = ((FloatingText) (event.getSource())).getTranslateY();
        });
        ft.setOnMouseDragged(event -> {
            double offsetX = event.getSceneX()*(1.0/zoomFactor.doubleValue()) - orgSceneX;
            double offsetY = event.getSceneY()*(1.0/zoomFactor.doubleValue()) - orgSceneY;  //*multiplier
            double newTranslateX = orgTranslateX + offsetX;
            double newTranslateY = orgTranslateY + offsetY;

            Bounds paneBounds = MainPane.getBoundsInLocal();

            if(newTranslateX > 0 && (newTranslateX+ft.getLayoutBounds().getWidth()) < paneBounds.getWidth())
            {
                System.out.println("x:"+newTranslateX+" max:"+paneBounds.getWidth());
                ((FloatingText) (event.getSource())).setTranslateX(newTranslateX);
            }
            if(newTranslateY > 0 && (newTranslateY+ft.getLayoutBounds().getHeight()) < paneBounds.getHeight())
            {
                ((FloatingText) (event.getSource())).setTranslateY(newTranslateY);
                //System.out.println("y:"+newTranslateY+" "+(ft.getLayoutBounds().getHeight())+" max:"+paneBounds.getHeight());
            }

        });
    }
    private void addDragAndDropFloatingTextBox(FloatingTextBox ft){
        ft.setOnMousePressed(event -> {

            orgSceneX = event.getSceneX()*(1.0/zoomFactor.doubleValue());//
            orgSceneY = event.getSceneY()*(1.0/zoomFactor.doubleValue());//*multiplier

            orgTranslateX = ((FloatingTextBox)(event.getSource())).getTranslateX();
            orgTranslateY = ((FloatingTextBox) (event.getSource())).getTranslateY();
        });
        ft.setOnMouseDragged(event -> {
            double offsetX = event.getSceneX()*(1.0/zoomFactor.doubleValue()) - orgSceneX;
            double offsetY = event.getSceneY()*(1.0/zoomFactor.doubleValue()) - orgSceneY;  //*multiplier
            double newTranslateX = orgTranslateX + offsetX;
            double newTranslateY = orgTranslateY + offsetY;

            Bounds paneBounds = MainPane.getBoundsInLocal();

            if(newTranslateX > 0 && (newTranslateX+ft.getLayoutBounds().getWidth()) < paneBounds.getWidth())
            {
                System.out.println("x:"+newTranslateX+" max:"+paneBounds.getWidth());
                ((FloatingTextBox) (event.getSource())).setTranslateX(newTranslateX);
            }
            if(newTranslateY > 0 && (newTranslateY+ft.getLayoutBounds().getHeight()) < paneBounds.getHeight())
            {
                ((FloatingTextBox) (event.getSource())).setTranslateY(newTranslateY);
                //System.out.println("y:"+newTranslateY+" "+(ft.getLayoutBounds().getHeight())+" max:"+paneBounds.getHeight());
            }

        });
    }
    public void addBackGroundImageWithoutZoom(Image image){
        BackGroundPane.getChildren().clear();
        backGroundImageView= new ImageView(image);
        System.out.println("height: "+this.getPaneHeight()+" width: "+this.getPaneWidth());
        backGroundImageView.setFitHeight(this.getPaneHeight());
        backGroundImageView.setFitWidth(this.getPaneWidth());
        //backGroundImageView.setPreserveRatio(true);
        backGroundImageView.setOpacity(0.7);
        BackGroundPane.getChildren().add(backGroundImageView);
        backGroundImageView.setLayoutX(0);
        backGroundImageView.setLayoutY(0);


    }
    public void addBackGroundImage(Image image){
        System.out.println("img h:"+image.getHeight()+" img w:"+image.getWidth());

        BackGroundPane.getChildren().clear();
        backGroundImageView= new ImageView(image);
        backGroundImageView.setPreserveRatio(true);
        if(this.getPaneHeight()>this.getPaneWidth()){
            backGroundImageView.setFitHeight(getPaneHeight());
            MIN_PIXELS= this.getPaneWidth();

        }
        else{
            backGroundImageView.setFitWidth(getPaneWidth());
            MIN_PIXELS=this.getPaneHeight();
        }
        backGroundImageView.setSmooth(true);

        Rectangle rectangle = new Rectangle(0,0,(double)getPaneWidth(),(double)getPaneHeight());
        Rectangle2D rectangle2D = new Rectangle2D(0,0,getPaneWidth(),getPaneHeight());
        BackGroundPane.getChildren().add(backGroundImageView);

        backGroundImageView.setLayoutX(0);
        backGroundImageView.setLayoutY(0);
        backGroundImageView.setOpacity(0.6);
        //backGroundImageView.setClip(rectangle);
        backGroundImageView.setViewport(rectangle2D);

        height = this.getPaneHeight();
        width = this.getPaneWidth();
        zoomEffect();
        reset(backGroundImageView, width / 4, height/4 );

    }
    protected void zoomEffect(){

        ObjectProperty<Point2D> mouseDown = new SimpleObjectProperty<>();
        //reset(backGroundImageView, width/2 , height/2 );
        backGroundImageView.setOnMousePressed(e -> {
            Point2D mousePress = imageViewToImage(backGroundImageView, new Point2D(e.getX(), e.getY()));
            mouseDown.set(mousePress);
        });
        backGroundImageView.setOnMouseDragged(e -> {
            Point2D dragPoint = imageViewToImage(backGroundImageView, new Point2D(e.getX(), e.getY()));
            shift(backGroundImageView, dragPoint.subtract(mouseDown.get()));
            mouseDown.set(imageViewToImage(backGroundImageView, new Point2D(e.getX(), e.getY())));
        });
        backGroundImageView.setOnScroll(e -> {
            double delta = e.getDeltaY();
            Rectangle2D viewport = backGroundImageView.getViewport();

            /*if(this.getPaneWidth()<this.getPaneHeight()){
                MIN_PIXELS=this.getPaneWidth();
            }
            else{
                MIN_PIXELS=this.getPaneHeight();
            } */

            double scale;
            //if(this.getPaneWidth()<this.getPaneHeight()){
            scale= clamp(Math.pow(1.01, delta),




                    // don't scale so we're zoomed in to fewer than MIN_PIXELS in any direction:
                    Math.min(MIN_PIXELS / viewport.getWidth(), MIN_PIXELS / viewport.getHeight()),

                    // don't scale so that we're bigger than image dimensions:
                    Math.max(this.getPaneWidth() / viewport.getWidth(), this.getPaneHeight() / viewport.getHeight())

            );
            /*}
            else{
                scale= clamp(Math.pow(1.01, delta),




                        // don't scale so we're zoomed in to fewer than MIN_PIXELS in any direction:
                        Math.min(MIN_PIXELS / viewport.getHeight(), MIN_PIXELS / viewport.getWidth()),

                        // don't scale so that we're bigger than image dimensions:
                        Math.max(this.getPaneWidth() / viewport.getWidth(), this.getPaneHeight() / viewport.getHeight())

                );
            }*/

            Point2D mouse = imageViewToImage(backGroundImageView, new Point2D(e.getX(), e.getY()));

            double newWidth = viewport.getWidth() * scale;
            double newHeight = viewport.getHeight() * scale;

            // To keep the visual point under the mouse from moving, we need
            // (x - newViewportMinX) / (x - currentViewportMinX) = scale
            // where x is the mouse X coordinate in the image

            // solving this for newViewportMinX gives

            // newViewportMinX = x - (x - currentViewportMinX) * scale

            // we then clamp this value so the image never scrolls out
            // of the imageview:

            double newMinX = clamp(mouse.getX() - (mouse.getX() - viewport.getMinX()) * scale,
                    0, width - newWidth);
            double newMinY = clamp(mouse.getY() - (mouse.getY() - viewport.getMinY()) * scale,
                    0, height - newHeight);

            backGroundImageView.setViewport(new Rectangle2D(newMinX, newMinY, newWidth, newHeight));
        });

        backGroundImageView.setOnMouseClicked(e -> {
            if (e.getClickCount() == 2) {

                reset(backGroundImageView, width/2, height/2);
                if(this.getPaneWidth()<this.getPaneHeight()) {
                    backGroundImageView.setFitHeight(this.getPaneHeight());
                }
                else{
                    backGroundImageView.setFitWidth(this.getPaneWidth());
                }
            }
        });



    }
    private void reset(ImageView iView, double width, double height) {
        iView.setViewport(new Rectangle2D(0, 0, width, height));
    }
    private Point2D imageViewToImage(ImageView iView, Point2D imageViewCoordinates) {
        double xProportion = imageViewCoordinates.getX() / iView.getBoundsInLocal().getWidth();
        double yProportion = imageViewCoordinates.getY() / iView.getBoundsInLocal().getHeight();

        Rectangle2D viewport = iView.getViewport();
        return new Point2D(
                viewport.getMinX() + xProportion * viewport.getWidth(),
                viewport.getMinY() + yProportion * viewport.getHeight());
    }
    private double clamp(double value, double min, double max) {

        if (value < min)
            return min;
        if (value > max)
            return max;
        return value;
    }


    private void shift(ImageView imageView, Point2D delta) {
        Rectangle2D viewport = imageView.getViewport();
        double width = imageView.getImage().getWidth();
        double height = imageView.getImage().getHeight();
        double maxX = width - viewport.getWidth();
        double maxY = height - viewport.getHeight();
        double minX = clamp(viewport.getMinX() - delta.getX(), 0, maxX);
        double minY = clamp(viewport.getMinY() - delta.getY(), 0, maxY);
        if (minX < 0.0) {
            minX = 0.0;
        }
        if (minY < 0.0) {
            minY = 0.0;
        }
        imageView.setViewport(new Rectangle2D(minX, minY, viewport.getWidth(), viewport.getHeight()));
    }

    public void imageViewAddDragAndDrop(ResizeableImageView iv){
        iv.setOnMousePressed(event -> {
            orgSceneX = event.getSceneX()*(1.0/zoomFactor.doubleValue());
            orgSceneY = event.getSceneY()*(1.0/zoomFactor.doubleValue());
            orgTranslateX = ((ResizeableImageView)(event.getSource())).getTranslateX();
            orgTranslateY = ((ResizeableImageView) (event.getSource())).getTranslateY();
        });
        iv.setOnMouseDragged(event -> {
            double offsetX =  event.getSceneX()*(1.0/zoomFactor.doubleValue())-orgSceneX;
            double offsetY =  event.getSceneY()*(1.0/zoomFactor.doubleValue())-orgSceneY ;
            double newTranslateX = orgTranslateX + offsetX;
            double newTranslateY = orgTranslateY + offsetY;
            ((ResizeableImageView) (event.getSource())).setTranslateX(newTranslateX);
            ((ResizeableImageView) (event.getSource())).setTranslateY(newTranslateY);

        });
    }


    private void editPane() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("editmypane.fxml"));
        Parent root = loader.load();

        System.out.println("edit 2.1");

        EditMyPane controller = loader.getController();
        System.out.println("edit 2.2");
        System.out.println(getPane());
        controller.initData(getPane());
        System.out.println("edit 2.3");
        loader.setRoot(EditMyPane.class);
        System.out.println("edit 2.4");
        loader.setController(EditMyPane.class);

        Stage panestage = new Stage(StageStyle.DECORATED);

        panestage.initModality(Modality.APPLICATION_MODAL);
        panestage.setTitle("Oldal szerkesztése");
        panestage.setScene(new Scene(root));

        panestage.showAndWait();
        if(!this.getChildren().contains(MainPane)){
            this.getChildren().add(MainPane);
        }
    }

    public double getZoomFactor() {
        return zoomFactor.get();
    }

    public DoubleProperty zoomFactorProperty() {
        return zoomFactor;
    }



    public MyPane(Node... children) {
        super(children);
    }


    @Override
    public ObservableList<Node> getChildren() {
        return super.getChildren();
    }



    public void placeImageViewOnPane(ResizeableImageView iv){
        MainPane.getChildren().add(iv);
        addContextMenu(iv);
        imageViewAddDragAndDrop(iv);
        //DragResizeMod.makeResizable(iv);

    }
    public void placeFloatingTextOnPane(FloatingText ft,int x, int y){
        MainPane.getChildren().add(ft);
        addContextMenuFloatingText(ft);
        addDragAndDropFloatingText(ft);
        ft.setTranslateX(x);
        ft.setTranslateY(y);

    }
    public void placeFloatingTextBoxOnPane(FloatingTextBox ft,int x, int y){
        MainPane.getChildren().add(ft);
        addContextMenuFloatingTextBox(ft);
        addDragAndDropFloatingTextBox(ft);
        ft.setTranslateX(x);
        ft.setTranslateY(y);

    }


    private void addContextMenu(ResizeableImageView iv){
        iv.setOnContextMenuRequested(event -> {
            //if(event.getTarget() instanceof ResizeableImageView || event.getTarget() instanceof Image){
            ResizeableImageViewContextMenu contextMenu = new ResizeableImageViewContextMenu(iv);
            contextMenu.show(iv,event.getScreenX(),event.getScreenY());
            //}

        });
    }
    private void addContextMenuFloatingText(FloatingText ft){
        ft.setOnContextMenuRequested(event -> {
            //if(event.getTarget() instanceof FloatingText){
            FloatingTextContextMenu textContextMenu = new FloatingTextContextMenu(ft);
            textContextMenu.show(ft,event.getScreenX(),event.getScreenY());
            //}

        });
    }
    private void addContextMenuFloatingTextBox(FloatingTextBox ft){
        ft.setOnContextMenuRequested(event -> {
            //if(event.getTarget() instanceof FloatingText){
            FloatingTextBoxContextMenu textContextMenu = new FloatingTextBoxContextMenu(ft);
            textContextMenu.show(ft,event.getScreenX(),event.getScreenY());
            //}

        });
    }
    private void rotateImageView(ResizeableImageView iv){
        class MovedHandler implements EventHandler<MouseEvent> {
            double startX;
            double startRotate;
            @Override
            public void handle(MouseEvent event) {
                Point2D pt = iv.localToParent(event.getX(), event.getY());
                double x = pt.getX();
                double newRotate = (x - startX) + startRotate;
                iv.setRotate(newRotate);
            }
        }

        MovedHandler handler = new MovedHandler();
        iv.setOnMouseDragged(handler);
        iv.setOnMousePressed(event -> {
            Point2D pt = iv.localToParent(event.getX(), event.getY());
            handler.startX = pt.getX();
            handler.startRotate = iv.getRotate();
        });
    }
    private void noDragResize(ResizeableImageView iv)  //mousePressed , mouseDragged, mouseOver, mouseRelesed
    {

        iv.setOnMouseMoved(event -> {

        });
        iv.setOnMouseReleased(event -> {

        });
    }


    private class MyPaneContextMenu extends ContextMenu {
        public MyPaneContextMenu(Pane myPane){
            MenuItem editpane = new MenuItem("Oldal szerkesztése");
            editpane.setOnAction(event ->
            {
                try {


                    Group mynode = (Group)getPane().getParent();
                    MyPane.this.setInEditMode(true);

                    editPane();

                    MyPane.this.setInEditMode(false);
                    mynode.getChildren().add(getPane());
                    //backGroundImageView.setOpacity(1.0);


                }
                catch (Exception e)
                {
                    System.out.println("Hiba az oldal szerkesztésénél");
                    System.out.println(e.toString());

                }
            });

            getItems().add(editpane);
        }
    }
    private class FloatingTextContextMenu extends ContextMenu{
        public FloatingTextContextMenu(FloatingText ft){
            MenuItem delete = new MenuItem("Szöveg törlése");
            delete.setOnAction(event -> {
                MainPane.getChildren().remove(ft);
            });
            MenuItem uptop = new MenuItem("Szöveg előre helyezése");
            uptop.setOnAction(event -> {
                ft.toFront();
            });
            MenuItem edittext = new MenuItem("Szöveg szerkesztése");
            edittext.setOnAction(event -> {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("editmypane.fxml"));
                EditMyPane controller = loader.getController();
                controller.editText(ft);
            });

            this.setStyle("-fx-font-size: 12;");
            if(inEditMode){
                getItems().addAll(edittext);
            }


            getItems().addAll(uptop,new SeparatorMenuItem(),delete);
        }

    }
    private class FloatingTextBoxContextMenu extends ContextMenu{
        public FloatingTextBoxContextMenu(FloatingTextBox ft){
            MenuItem delete = new MenuItem("Szöveg törlése");
            delete.setOnAction(event -> {
                MainPane.getChildren().remove(ft);
            });
            MenuItem uptop = new MenuItem("Szövegdoboz előre helyezése");
            uptop.setOnAction(event -> {
                ft.toFront();
            });
            /*MenuItem edittext = new MenuItem("Szöveg szerkesztése");
            edittext.setOnAction(event -> {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("editmypane.fxml"));
                EditMyPane controller = loader.getController();
                controller.editText(ft);
            });*/

            this.setStyle("-fx-font-size: 12;");
            /*if(inEditMode){
                getItems().addAll(edittext);
            }*/


            getItems().addAll(uptop,new SeparatorMenuItem(),delete);
        }

    }

    private class ResizeableImageViewContextMenu extends ContextMenu {
        public ResizeableImageViewContextMenu(ResizeableImageView iv) {
            MenuItem removeImageView = new MenuItem("Kép eltávolítása");
            removeImageView.setOnAction(event -> {
                MainPane.getChildren().remove(iv);

            });
            RadioMenuItem resize = new RadioMenuItem("Kép átméretezése");
            resize.setOnAction(event -> {

                DragResizeMod.makeResizable(iv);

            });
            RadioMenuItem rotate = new RadioMenuItem("Kép forgatása");
            rotate.setOnAction(event -> {
                noDragResize(iv);
                rotateImageView(iv);

            });
            RadioMenuItem move = new RadioMenuItem("Kép mozgatása");
            move.setOnAction(event -> {
                noDragResize(iv);
                imageViewAddDragAndDrop(iv);
            });

            MenuItem uptop = new MenuItem("Kép előre helyezése");
            uptop.setOnAction(event -> {
                iv.toFront();
            });

            MenuItem addframe = new MenuItem("Keret hozzáadás");

            addframe.setOnAction(event -> {
                //Window stage = ((MenuItem) event.getTarget()).getParentPopup().getOwnerWindow();
                //this.hide();
                System.out.println("keret 1");
                try{
                    System.out.println("keret 1");
                    addFrameToImage(iv);
                }
                catch (Exception e)
                {
                    System.out.println(e);
                }



            });
            MenuItem bgImage = new MenuItem("Kép Háttérnek");
            bgImage.setOnAction(event -> {
                addBackGroundImage(iv.getImageView().getImage());
            });
            MenuItem circle = new MenuItem("Kör");
            MenuItem ellipsis = new MenuItem("Ellipszis");
            MenuItem rectangle = new MenuItem("Négyszög");
            MenuItem heart = new MenuItem("heart");
            MenuItem triangledown = new MenuItem("Háromszög - lefele");
            MenuItem triangleup = new MenuItem("Háromszög - felfele");
            Menu shapeMenu=new Menu("Formák",null,rectangle,circle,ellipsis,triangledown,triangleup); //heart

            rectangle.setOnAction(event -> {
                shapeCreator.addShapeToImageView(iv,0);
            });
            circle.setOnAction(event -> {
                shapeCreator.addShapeToImageView(iv,1);
            });
            ellipsis.setOnAction(event -> {
                shapeCreator.addShapeToImageView(iv,2);
            });
            heart.setOnAction(event -> {
                shapeCreator.addShapeToImageView(iv,3);
            });
            triangledown.setOnAction(event -> {
                shapeCreator.addShapeToImageView(iv,4);
            });
            triangleup.setOnAction(event -> {
                shapeCreator.addShapeToImageView(iv,5);



            });

            addframe.setOnAction(event -> {
                addFrameToImage(iv);

            });
            MenuItem addPhoto = new MenuItem("Kép hozzáadása");
            addPhoto.setOnAction(event -> {

            });

            ToggleGroup tg = new ToggleGroup();
            move.setToggleGroup(tg);
            rotate.setToggleGroup(tg);
            resize.setToggleGroup(tg);

            getItems().addAll(resize,rotate,move,new SeparatorMenuItem(),uptop,new SeparatorMenuItem()); //
            if(iv.isImageView())
            {
                getItems().addAll(shapeMenu,bgImage,new SeparatorMenuItem());
            }
            if(!inEditMode && iv.isImageView() ){
                SeparatorMenuItem frameSeparator = new SeparatorMenuItem();
                if(iv.getShapeType()=="null"){
                    getItems().addAll(addframe,frameSeparator); //
                }
                else {
                    getItems().removeAll(addframe,frameSeparator);
                }

            }
            else{
                if(iv.getImageView()==null){
                    getItems().addAll(addPhoto,new SeparatorMenuItem());
                }
            }

            getItems().add(removeImageView);
        }
    }
    public void addFrameToImage(ResizeableImageView iv){

        BorderPane root =(BorderPane) getParent().getParent().getParent().getParent().getParent();
        VBox borderBox = (VBox)root.lookup("#borderBox");
        borderBox.visibleProperty().setValue(true);
        Slider borderSlider = (Slider)root.lookup("#borderSlider");
        ColorPicker borderColorPicker = (ColorPicker)root.lookup("#borderColorPicker");
        Button allImages = (Button)root.lookup("#allImages");
        Button cancelFrames = (Button)root.lookup("#cancelFrames");
        Button saveFrames = (Button)root.lookup("#saveFrames");
        //private Border imageBorder;
        //private Color imageBorderColor;
        //private BorderWidths imageBorderWidths;

        System.out.println("border 1");
        iv.setEditable(true);
        if(iv.getBorder()!=null){
            borderSlider.valueProperty().setValue(iv.getBorderWidth());
            borderColorPicker.setValue(iv.getBorderColor());
        }
        if(iv.getEditable()) {

            borderColorPicker.valueProperty().addListener(((observable, oldValue, newValue) -> {
                System.out.println(newValue.toString());
                imageBorderColor = newValue;

                iv.setImageBorder(new Border(new BorderStroke(newValue, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, imageBorderWidths, new Insets(0, 0, 0, 0))));
                iv.setBorderColor(newValue);

            }));
            borderSlider.valueProperty().addListener(((observable, oldValue, newValue) -> {
                System.out.println(newValue.doubleValue());

                imageBorderWidths = new BorderWidths(newValue.doubleValue(), newValue.doubleValue(), newValue.doubleValue(), newValue.doubleValue());
                //imageBorder =
                iv.setBorderWidth(newValue.doubleValue());
                iv.setImageBorder(new Border(new BorderStroke(imageBorderColor, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, imageBorderWidths, new Insets(0, 0, 0, 0))));

            }));
            allImages.setOnAction(event -> {
                for (ResizeableImageView ri : imageViewList) {
                    if(ri.getShapeType()=="null"){
                        ri.setBorder(new Border(new BorderStroke(imageBorderColor, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, imageBorderWidths, new Insets(0, 0, 0, 0))));
                    }

                }

            });

            cancelFrames.setOnAction(event -> {
                iv.setBorder(null);
                for(ResizeableImageView ri : imageViewList){
                    ri.setBorder(null);
                }
                iv.setEditable(false);
                borderBox.visibleProperty().setValue(false);
            });
            saveFrames.setOnAction(event -> {
                borderBox.visibleProperty().setValue(false);
                iv.setEditable(false);
            });
        }

    }
}