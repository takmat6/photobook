package sample;

public class Page{
    String type;
    public int x;
    public int y;

    public Page(String type,int x, int y) {
        this.type=type;
        this.x = x;
        this.y = y;
    }


    public String getType() {
        return type;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public String toString() {
        return type+" x="+x+", y="+y;
    }
}
