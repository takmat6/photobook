package sample;


public enum PageSizes
{
    A4("A4",992,1403),
    A5("A5",699,992),
    B3("B3",1668,2362),
    Teszt("teszt",1250,1400);


    private final String type;
    private final int x;
    private final int y;
    PageSizes(String type,int x,int y){
        this.type=type;
        this.x=x;
        this.y=y;
    }

    public String getType() {
        return type;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public String toString() {
        switch (this){
            case A4:
                return A4.getType()+" - sz:"+A4.getX()+" m:"+A4.getY();
            case A5:
                return A5.getType()+" - sz:"+A5.getX()+" m:"+A5.getY();
            case B3:
                return B3.getType()+" - sz:"+B3.getX()+" m:"+B3.getY();
            case Teszt:
                return Teszt.getType()+" - sz:"+Teszt.getX()+" m:"+Teszt.getY();

        }
        return "Hiba";
    }

}
