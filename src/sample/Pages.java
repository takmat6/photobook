package sample;

import java.util.ArrayList;

public interface Pages {
    ArrayList<MyPane> listOfPanes = new ArrayList<>();
    ArrayList<ThemeDisplay> listOfThemes = new ArrayList<>();
    ArrayList<TextBoxImage> listOfTextBoxImages = new ArrayList<>();

}
