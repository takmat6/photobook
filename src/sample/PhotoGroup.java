package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableMap;
import javafx.util.Pair;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class PhotoGroup {


    private String name;
    private Integer numberOfImages;


    public PhotoGroup(String name){
        this.name = name;
        this.numberOfImages=0;


    }

    public Integer getNumberOfImages() {
        return numberOfImages;
    }

    public void setNumberOfImages(Integer numberOfImages) {
        this.numberOfImages = numberOfImages;
    }

    public String getName() {
        return name;
    }



    protected void addImageToGroup(){

    }


}
