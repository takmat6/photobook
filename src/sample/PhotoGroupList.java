package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public interface PhotoGroupList {
    ObservableList<PhotoGroup> groupList = FXCollections.observableArrayList();

}
