package sample;

import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.MapChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import java.util.ArrayList;

public class PhotoGroups implements PhotoGroupList {
    @FXML
    Button showNewGroup = new Button();
    @FXML
    HBox newGroupBox ;
    @FXML
    TextField newGroupText = new TextField();

    @FXML
    Label isSelected = new Label();
    @FXML
    BorderPane groupRoot = new BorderPane();

    @FXML
    Button closeButton = new Button();
    @FXML
    ScrollPane groupScrollPane = new ScrollPane();
    TableView<PhotoGroup> tableView;
    PhotoView pv ;


    public String getSelectedGroup() {
        return selectedGroup;
    }

    public void setSelectedGroup(String selectedGroup) {
        this.selectedGroup = selectedGroup;
    }

    @FXML
    String selectedGroup = null;



     @FXML
     private void initialize(){
         init();

     }

     public void initData(PhotoView iv ){
            tableView.setPrefHeight(180);
            pv = iv;


     }
    public void showGroupBox(){
        if(!newGroupBox.isVisible())
        newGroupBox.setVisible(true);


    }

    public String getGroupOfImage(){
        return selectedGroup;
    }
    private void init(){
         if(pv!=null){
             isSelected.setText("A képnek már van csoportja ("+pv.getPhotoGroup()+")!");
         }
        TableColumn<PhotoGroup, String> nameOfGroups = new TableColumn<>("Csoport neve");
        TableColumn<PhotoGroup, Integer> numberOfImages = new TableColumn<>("Képek száma");
        nameOfGroups.setMinWidth(150);
        numberOfImages.setMinWidth(150);
        nameOfGroups.setCellValueFactory(new PropertyValueFactory<>("name"));
        numberOfImages.setCellValueFactory(new PropertyValueFactory<>("numberOfImages"));


        tableView = new TableView<>();
        tableView.setItems(groupList);
        tableView.getColumns().addAll(nameOfGroups,numberOfImages);
        groupScrollPane.setContent(tableView);
        tableView.setRowFactory( tv -> {
            TableRow<PhotoGroup> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if(event.getClickCount()==2 && (!row.isEmpty())){
                    PhotoGroup pg = row.getItem();
                    setSelectedGroup(pg.getName());
                    isSelected.setText("Csoport kiválasztva: "+getSelectedGroup());
                }
            });
            return row;
        });
        //tableView.refresh();

    }

    public void createNewGroup(){
        String group =newGroupText.getText();

        addGroup(group);

        //tableView.refresh();

        //fillTableView();


    }

    private void addGroup(String name){
         PhotoGroup newPG = new PhotoGroup(name);
        if(!groupList.contains(newPG)){
            groupList.add(newPG);
        }
        else{ System.out.println("Csoport már létezik!");}

        newGroupText.setText("");

    }

    public void closeButtonAction(){

        Stage stage = (Stage) closeButton.getScene().getWindow();
        for(PhotoGroup pg : groupList){
            if(pg.getName().equals(selectedGroup)){
                pg.setNumberOfImages(pg.getNumberOfImages()+1);
            }
        }

        stage.close();
    }
}
