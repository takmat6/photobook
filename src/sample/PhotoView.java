package sample;


import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.*;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

public class PhotoView extends StackPane {
    private int ID;

    @FXML
    Label GroupOfPhoto;
    @FXML
    Tooltip groupTooltip;
    @FXML
    Label OnPages;
    @FXML
    ImageView PhotoView;
    @FXML
    VBox textVbox;
    @FXML
    CheckBox isImportant;
    boolean importantImage;
    boolean isOnPane=false;
    String photoGroup;
    private int onThePane;

    public int getOnThePane() {
        return onThePane;
    }

    public void setOnThePane(int onThePane) {
        this.onThePane = onThePane;
    }

    public boolean isImportantImage() {
        return importantImage;
    }



    public String getPhotoGroup() {
        return photoGroup;
    }

    public void setPhotoGroup(String photoGroup) {
        this.GroupOfPhoto.setText(photoGroup);
        this.groupTooltip.setText(photoGroup);
        this.photoGroup = photoGroup;
        if(OnPages.getText()!="" || GroupOfPhoto.getText()!=""){
            textVbox.setStyle("-fx-background-color: #ffffff;");
            textVbox.getChildren().remove(isImportant);
        }
    }
    public void setImportantVisibility(boolean important){
        if(important) {
            importantImage=(important);
            textVbox.setStyle("-fx-background-color: #ffffff;");
        }
        else{
            isImportant.setVisible(false);
            if(OnPages.getText()=="" || GroupOfPhoto.getText()==""){
                textVbox.setStyle(null);
            }
        }
    }

    String pathOfFile;
    public String getPathOfFile() {
        return pathOfFile;
    }

    public void setPathOfFile(String pathOfFile) {
        this.pathOfFile = pathOfFile;
    }

    public boolean isOnPane() {
        return isOnPane;
    }

    public void setOnPane(boolean onPane) {
        isOnPane = onPane;
    }

    public void setOnPages(int onPages) {
        OnPages.setText(onPages+". oldalon");
        onThePane=onPages;
        if(OnPages.getText()!="" || GroupOfPhoto.getText()!=""){
            textVbox.setStyle("-fx-background-color: #ffffff;");

        }
    }

    public PhotoView(int ID) {
        //super();
        //this.PhotoView = new ImageView();
        loadUI();

        this.ID= ID;
        setDragAndDrop();
        //importantImage=new SimpleBooleanProperty(false);

        importantImage=false;
        isImportant.selectedProperty().addListener((observable, oldValue, newValue) ->{
            if(newValue){
                importantImage=true;}

                else{
                    importantImage=false;

        }

        });
        //isImportant.visibleProperty().bind(importantImage);


    }

    public PhotoView(String url,int ID) {

        loadUI();
        this.PhotoView.setImage(new Image(url));
        this.ID = ID;
        setDragAndDrop();
    }

    public PhotoView(Image image,int ID) {
        //super(image);
        loadUI();
        this.PhotoView.setImage(image);
        this.ID = ID;

        setDragAndDrop();
    }

    public int getID() {
        return ID;
    }
    private void loadUI(){
        FXMLLoader loader = new FXMLLoader(getClass().getResource("photoview.fxml"));
        loader.setRoot(this);
        loader.setController(this);
        try {
            loader.load();
        } catch (Exception e) {

        }
    }

    public ImageView getPhotoView() {
        return PhotoView;
    }

    public void setPhotoView(ImageView photoView) {
        PhotoView = photoView;
    }
    public Image getImage(){
        return this.getPhotoView().getImage();
    }

    public String saveAs(){
        return "[PHOTOVIEW]\n[PATH]"+pathOfFile+"\n[ID]"+getID()+"\n[ONPANE]"+isOnPane()+"\n[PANENUMBER]"+getOnThePane()+"\n[GROUP]"+GroupOfPhoto.getText()+"\n[PHOTOVIEW/]";
    }

    public void setPhoto(Image image){
        this.PhotoView.setImage(image);
    }

   public void setDragAndDrop(){
        this.setOnDragDetected(event -> {
            Dragboard db = this.startDragAndDrop(TransferMode.ANY);
            ClipboardContent content = new ClipboardContent();
            content.putString(this.pathOfFile);
            content.putImage(this.getPhotoView().getImage());
            db.setContent(content);
            event.consume();
        });
        this.setOnDragDone(event -> {
            if(event.getTransferMode()==TransferMode.MOVE)
                event.consume();
        });
   }
}
