package sample;

import java.util.ArrayList;
import java.util.List;

public interface PhotoViews {
    public ArrayList<PhotoView> photoViews=new ArrayList<>();
    public int idOfPhotoView=0;
    List<String> filePaths = new ArrayList<>();
    List<Integer> fileID = new ArrayList<>();
}
