package sample;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Border;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;

import javax.imageio.ImageIO;
import java.io.IOException;
import java.net.URL;


public class ResizeableImageView extends StackPane implements ResizeableImageViews{

    private final int MIN_PIXELS = 10;
    private double width ;
    private double height;
    private ImageView imageView;
    private Boolean editable = false;
    private Border imageBorder=null;
    private Shape imageShape;
    private String shapeType="null";
    private ImageView editLines;
    private boolean isImportant;
    private boolean isImageView=true;

    public boolean isImageView() {
        return isImageView;
    }

    public void setImageView(boolean imageView) {
        isImageView = imageView;
    }

    public boolean isImportant() {
        return isImportant;
    }

    public void setImportant(boolean important) {
        isImportant = important;
    }

    public String getShapeType() {
        return shapeType;
    }

    public void setShapeType(String shapeType) {
        this.shapeType = shapeType;
    }




    public Border getImageBorder() {
        return imageBorder;
    }

    public void setImageBorder(Border imageBorder) {
        this.imageBorder = imageBorder;
        if(editable){
            this.setBorder(imageBorder);
        }

    }
    /*private void loadEditTools(){
        FXMLLoader loader = new FXMLLoader(getClass().getResource("resizeableimageview.fxml"));
        loader.setRoot(ImageEditBox);
        loader.setController(ImageEditBox);

        try {
            loader.load();

        } catch (Exception e) {

        }
        System.out.println(ImageEditBox);

    }*/


    public Shape getImageShape() {
        return this.imageShape;
    }


    public void setImageShape(Shape shape) {
        this.imageShape = shape;
        this.setClip(shape);
    }

    public Boolean getEditable() {
        return editable;
    }

    public void setEditable(Boolean editable) {
        this.editable = editable;
    }

    private String pathOfImage;

    public String getPathOfImage() {
        return pathOfImage;
    }

    private Color borderColor;

    private double borderWidth;

    public double getBorderWidth() {
        return borderWidth;
    }

    public void setBorderWidth(double borderWidth) {
        this.borderWidth = borderWidth;
    }

    public Color getBorderColor() {
        return borderColor;
    }

    public void setBorderColor(Color borderColor) {
        this.borderColor = borderColor;
    }

    public ImageView getImageView() {
        return imageView;
    }

    public void setPathOfImage(String pathOfImage) {
        this.pathOfImage = pathOfImage;
    }

    public double getImageWidth() {
        return width;
    }

    public double getImageHeight() {
        return height;
    }



    public ResizeableImageView() {
        super();

    }

    public ResizeableImageView(String url) {
        super();
    }

    public void setBGColor(Color bg){

        this.setStyle("-fx-background-color: "+bg.toString().replace("0x","")+";");

    }
    /*private void rotateImageView(ImageView iv,ResizeableImageView riv){

        class MovedHandler implements EventHandler<MouseEvent> {

            double startX;
            double startRotate;

            @Override
            public void handle(MouseEvent event) {
                Point2D pt = riv.localToParent(event.getX(), event.getY());
                double x = pt.getX();
                double newRotate = (x - startX) + startRotate;
                riv.setRotate(newRotate);
            }

        }
        MovedHandler handler = new MovedHandler();
        iv.setOnMouseDragged(handler);
        iv.setOnMousePressed(event -> {
            Point2D pt = riv.localToParent(event.getX(), event.getY());
            handler.startX = pt.getX();
            handler.startRotate = riv.getRotate();
        });
    }*/

    public ResizeableImageView(Image image) {
        super();
        //loadEditTools();
        this.setAlignment(Pos.CENTER);
        Image outline = new Image(getClass().getResource("/sample/outline.png").toString());
        Image rotate = new Image(getClass().getResource("/sample/rotate.png").toString());

        ImageView rotateImage = new ImageView(rotate);
        //rotateImageView(rotateImage,this);
        editLines = new ImageView(outline);
        imageView = new ImageView(image);
        //this.getChildren().add(editLines);
        //editLines.setVisible(false);


        this.setOnMouseClicked(e -> {
            if (e.getClickCount() == 2) {
                if(this.getChildren().contains(editLines)){
                    this.getChildren().remove(editLines);

                    //this.getChildren().remove(ImageEditBox);
                    //this.getChildren().remove(rotateImage);
                }
                else{
                    this.getChildren().add(editLines);
                    //this.getChildren().add(rotateImage);
                    DragResizeMod.makeResizable(editLines);
                    //this.getChildren().add(ImageEditBox);
                    //DragResizeMod.makeResizable(Resize);
          //          rotateImageView(Rotate,this);
                    //editLines
                    //editLines.toBack();
                }
               /* if(editLines.isVisible()){
                    editLines.setVisible(false);
                }
                else{
                    editLines.setVisible(true);
                }*/
            }
        });



        width=image.getWidth();
        height=image.getHeight();
        imageView.setFitHeight(height);
        imageView.setFitWidth(width);
        imageView.setPreserveRatio(true);
        editLines.fitWidthProperty().bind(imageView.fitHeightProperty().add(40));
        editLines.fitHeightProperty().bind(imageView.fitHeightProperty().add(40));
        //this.prefHeightProperty().bind(imageView.fitHeightProperty());
        //this.prefWidthProperty().bind(imageView.fitWidthProperty());
        //editLines.setFitHeight(height+40);
        //editLines.setFitWidth(width+40);
        this.getChildren().add(imageView);

        imageViewList.add(this);


    }
    public void setImage(Image image){
        imageView = new ImageView(image);
        width=image.getWidth();
        height=image.getHeight();
        imageView.setFitHeight(height);
        imageView.setFitWidth(width);
        imageView.setPreserveRatio(true);
        this.getChildren().add(imageView);
    }

    public String saveAs(){

        return "[PATH]"+pathOfImage+"\n[WIDTH]"+(int)this.imageView.getFitWidth()+"\n[HEIGHT]"+(int)this.imageView.getFitHeight()+"\n"+"[BORDER]"+borderColor+";"+borderWidth+"\n";
    }

    protected void init(){

        ObjectProperty<Point2D> mouseDown = new SimpleObjectProperty<>();
        reset(this, width , height );
        this.setOnMousePressed(e -> {

            Point2D mousePress = imageViewToImage(this, new Point2D(e.getX(), e.getY()));
            mouseDown.set(mousePress);
        });

        this.setOnMouseDragged(e -> {
            Point2D dragPoint = imageViewToImage(this, new Point2D(e.getX(), e.getY()));
            shift(this, dragPoint.subtract(mouseDown.get()));
            mouseDown.set(imageViewToImage(this, new Point2D(e.getX(), e.getY())));
        });
        this.setOnScroll(e -> {
            double delta = e.getDeltaY();
            Rectangle2D viewport = this.imageView.getViewport();

            double scale = clamp(Math.pow(1.01, delta),

                    // don't scale so we're zoomed in to fewer than MIN_PIXELS in any direction:
                    Math.min(MIN_PIXELS / viewport.getWidth(), MIN_PIXELS / viewport.getHeight()),

                    // don't scale so that we're bigger than image dimensions:
                    Math.max(width / viewport.getWidth(), height / viewport.getHeight())

            );

            Point2D mouse = imageViewToImage(this, new Point2D(e.getX(), e.getY()));

            double newWidth = viewport.getWidth() * scale;
            double newHeight = viewport.getHeight() * scale;

            // To keep the visual point under the mouse from moving, we need
            // (x - newViewportMinX) / (x - currentViewportMinX) = scale
            // where x is the mouse X coordinate in the image

            // solving this for newViewportMinX gives

            // newViewportMinX = x - (x - currentViewportMinX) * scale

            // we then clamp this value so the image never scrolls out
            // of the imageview:

            double newMinX = clamp(mouse.getX() - (mouse.getX() - viewport.getMinX()) * scale,
                    0, width - newWidth);
            double newMinY = clamp(mouse.getY() - (mouse.getY() - viewport.getMinY()) * scale,
                    0, height - newHeight);

            this.imageView.setViewport(new Rectangle2D(newMinX, newMinY, newWidth, newHeight));
        });

        this.setOnMouseClicked(e -> {
            if (e.getClickCount() == 2) {
                reset(this, width, height);
            }
        });



    }
    private void reset(ResizeableImageView iView, double width, double height) {
        iView.imageView.setViewport(new Rectangle2D(0, 0, width, height));
    }
    private Point2D imageViewToImage(ResizeableImageView iView, Point2D imageViewCoordinates) {
        double xProportion = imageViewCoordinates.getX() / iView.getBoundsInLocal().getWidth();
        double yProportion = imageViewCoordinates.getY() / iView.getBoundsInLocal().getHeight();

        Rectangle2D viewport = iView.imageView.getViewport();
        return new Point2D(
                viewport.getMinX() + xProportion * viewport.getWidth(),
                viewport.getMinY() + yProportion * viewport.getHeight());
    }
    private double clamp(double value, double min, double max) {

        if (value < min)
            return min;
        if (value > max)
            return max;
        return value;
    }


    private void shift(ResizeableImageView iView, Point2D delta) {
        Rectangle2D viewport = iView.imageView.getViewport();

        double width = iView.imageView.getImage().getWidth() ;
        double height = iView.imageView.getImage().getHeight() ;

        double maxX = width - viewport.getWidth();
        double maxY = height - viewport.getHeight();

        double minX = clamp(viewport.getMinX() - delta.getX(), 0, maxX);
        double minY = clamp(viewport.getMinY() - delta.getY(), 0, maxY);

        iView.imageView.setViewport(new Rectangle2D(minX, minY, viewport.getWidth(), viewport.getHeight()));
    }

}
