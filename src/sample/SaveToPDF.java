package sample;

import com.itextpdf.text.Document;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfWriter;
import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.concurrent.Task;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.SnapshotParameters;


import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

public class SaveToPDF extends Task<Void> implements Pages{

    private File pdfFile;
    private DoubleProperty zoomFactor;
    private float paneWidth,paneHeight;

    public SaveToPDF(File pdfFile, DoubleProperty zoomFactor, float paneWidth, float paneHeight) {
        this.pdfFile = pdfFile;
        this.zoomFactor = zoomFactor;
        this.paneWidth = paneWidth;
        this.paneHeight = paneHeight;
    }

    @Override
    protected Void call() throws Exception {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
        if(pdfFile != null){


            try {
                ArrayList<Image> images = new ArrayList<>();
                FileOutputStream fos=null;
                com.itextpdf.text.Image image;
                long update = 0;




                for(int i = 0;i < listOfPanes.size();i++) {
                    listOfPanes.get(i).setZoomFactor(2d);
                    BufferedImage bufImage = SwingFXUtils.fromFXImage(listOfPanes.get(i).snapshot(new SnapshotParameters(), null), null);
                    FileOutputStream out = new FileOutputStream(new File("./temp.png"));
                    javax.imageio.ImageIO.write(bufImage, "png", out);
                    out.flush();
                    out.close();
                    image = com.itextpdf.text.Image.getInstance("./temp.png");

                    images.add(image);
                    listOfPanes.get(i).setZoomFactor(zoomFactor.doubleValue());

                    //updateProgress(update,listOfPanes.size()*2);
                    update++;
                }
                Document doc = new Document(new com.itextpdf.text.Rectangle(paneWidth*2, paneHeight*2));
                fos = new FileOutputStream(pdfFile);
                PdfWriter.getInstance(doc, fos);
                doc.open();
                for(com.itextpdf.text.Image img : images){
                    img.setAbsolutePosition(0, 0);
                    doc.add(img);
                    doc.newPage();
                    fos.flush();

                    updateProgress(update,listOfPanes.size());
                    update++;
                }
                System.out.println(update);
                doc.close();
                fos.close();


            }
            catch (Exception e) {
                e.printStackTrace();
            }


        }


            }
        });
        return null;
    }

    @Override
    protected void cancelled() {
        super.cancelled();
    }

    @Override
    protected void updateProgress(double workDone, double max) {
        updateMessage(workDone +"/"+max);
        super.updateProgress(workDone, max);
    }
}
