package sample;

import javafx.scene.layout.Pane;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Shape;

public class ShapeCreator {
    public ShapeCreator() {
    }
    public void addShapeToImageView(ResizeableImageView iv,int type){
        switch(type){
            case 0:
                iv.setImageShape(null);
                break;
            case 1:
                //double radius =(iv.getImageView().getFitWidth()>iv.getImageView().getFitHeight())?iv.getImageView().getFitHeight()/2:iv.getImageView().getFitWidth();
                Circle circle = new Circle(); //radius
                //circle.centerXProperty().bind(iv.getImageView().fitWidthProperty().multiply(0.5));
                //circle.centerYProperty().bind(iv.getImageView().fitHeightProperty().multiply(0.5));
                //circle.radiusProperty().bind((iv.getImageView().getFitWidth()>iv.getImageView().getFitHeight())?iv.getImageView().fitHeightProperty().multiply(0.5):iv.getImageView().fitWidthProperty().multiply(0.5));

                circle.centerXProperty().bind(iv.widthProperty().multiply(0.5));
                circle.centerYProperty().bind(iv.heightProperty().multiply(0.5));
                circle.radiusProperty().bind((iv.getImageView().getFitWidth()>iv.getImageView().getFitHeight())?iv.heightProperty().multiply(0.5):iv.widthProperty().multiply(0.5));

                iv.setImageShape(circle);
                iv.setShapeType("circle");
                break;
            case 2:
                //Ellipse ellipse = new Ellipse(iv.getImageView().getFitWidth()/2,iv.getImageView().getFitHeight()/2,iv.getImageView().getFitWidth()/2,iv.getImageView().getFitHeight()/2);//iv.getImageView().getImage().getWidth()/2,iv.getImageView().getImage().getHeight()/2, iv.getImageView().getImage().getWidth()/2,iv.getImageView().getImage().getHeight()/2
                Ellipse ellipse = new Ellipse();
                //ellipse.centerXProperty().bind(iv.getImageView().fitWidthProperty().multiply(0.5));
                //ellipse.centerYProperty().bind(iv.getImageView().fitHeightProperty().multiply(0.5));
                //ellipse.radiusXProperty().bind(iv.getImageView().fitWidthProperty().multiply(0.5));
                //ellipse.radiusYProperty().bind(iv.getImageView().fitHeightProperty().multiply(0.5));

                ellipse.centerXProperty().bind(iv.widthProperty().multiply(0.5));
                ellipse.centerYProperty().bind(iv.heightProperty().multiply(0.5));
                ellipse.radiusXProperty().bind(iv.widthProperty().multiply(0.5));
                ellipse.radiusYProperty().bind(iv.heightProperty().multiply(0.5));

                iv.setImageShape(ellipse);
                iv.setShapeType("ellipse");
                break;
            case 3:
                Circle c1 = new Circle(iv.getImageView().getImage().getHeight()/4);
                Circle c2 = new Circle(iv.getImageView().getImage().getHeight()/4);

                c1.centerXProperty().bind(iv.widthProperty().multiply(0.33));
                c1.centerYProperty().bind(iv.heightProperty().multiply(0.25));
                c1.radiusProperty().bind(iv.getImageView().fitHeightProperty().multiply(0.25));
                c2.centerXProperty().bind(iv.widthProperty().multiply(0.67));
                c2.centerYProperty().bind(iv.heightProperty().multiply(0.25));
                c2.radiusProperty().bind(iv.getImageView().fitHeightProperty().multiply(0.25));
                Shape sh = Shape.union(c1,c2);
                Polygon excess = new Polygon();
                excess.getPoints().addAll(0.0,c1.getCenterY(),
                        0.0,c1.getCenterY()+c1.getRadius(),
                        iv.getImageView().getImage().getWidth(),c1.getCenterY()+c1.getRadius(),
                        iv.getImageView().getImage().getWidth(),c1.getCenterY());
                Shape shape2 = Shape.subtract(sh,excess);
                Polygon triangle = new Polygon();
                triangle.getPoints().addAll(c1.getCenterX()-c1.getRadius(), c1.getCenterY(),
                        iv.getImageView().getImage().getWidth()/2, iv.getImageView().getImage().getHeight(),
                        c2.getCenterX()+c1.getRadius(), c1.getCenterY()); //0.0,c1.getCenterY(),iv.getImageView().getFitWidth()/2,iv.getImageView().getFitHeight(),0.0,c2.getCenterY()

                Shape finalShape = Shape.union(shape2,triangle);
                iv.setImageShape(finalShape);



                break;
            case 4:
                Polygon triangleDown = new Polygon();
                triangleDown.getPoints().addAll(0.0,0.0,
                        iv.getWidth()/2, iv.getHeight(),
                        iv.getWidth(), 0.0); //0.0,c1.getCenterY(),iv.getImageView().getFitWidth()/2,iv.getImageView().getFitHeight(),0.0,c2.getCenterY()


                iv.setImageShape(triangleDown);
                iv.setShapeType("triangledown");
                break;
            case 5:
                Polygon triangleUp = new Polygon();
                triangleUp.getPoints().addAll(0.0,iv.getHeight(),
                        iv.getWidth()/2, 0.0,
                        iv.getWidth(), iv.getHeight()); //0.0,c1.getCenterY(),iv.getImageView().getFitWidth()/2,iv.getImageView().getFitHeight(),0.0,c2.getCenterY()


                iv.setImageShape(triangleUp);
                iv.setShapeType("triangleup");
                break;
        }

    }
    public void addShapeToImageView(ResizeableImageView iv,String type){
        switch(type){
            case "null":
                iv.setImageShape(null);
                break;
            case "circle":
                double radius =(iv.getImageView().getFitWidth()>iv.getImageView().getFitHeight())?iv.getImageView().getFitHeight()/2:iv.getImageView().getFitWidth();
                Circle circle = new Circle(radius);
                circle.centerXProperty().bind(iv.widthProperty().multiply(0.5));
                circle.centerYProperty().bind(iv.heightProperty().multiply(0.5));
                circle.radiusProperty().bind((iv.getImageView().getFitWidth()>iv.getImageView().getFitHeight())?iv.getImageView().fitHeightProperty().multiply(0.5):iv.getImageView().fitWidthProperty().multiply(0.5));
                iv.setImageShape(circle);
                iv.setShapeType("circle");
               /* Pane newPane = new Pane();
                newPane.setPrefWidth(iv.getImageView().getFitWidth()*1.2);
                newPane.setPrefHeight(iv.getImageView().getFitWidth()*1.2);
                newPane.setStyle("-fx-background-color: #000000;");
                Circle newCircle = new Circle(newPane.getPrefWidth()/2);
                newCircle.setCenterX(newPane.getPrefWidth()/2);
                newCircle.setCenterY(newPane.getPrefHeight()/2);
                //newPane.setClip(newCircle);
                iv.getChildren().add(newPane);
                newPane.toBack(); */
                break;
            case "ellipse":
                Ellipse ellipse = new Ellipse(iv.getImageView().getFitWidth()/2,iv.getImageView().getFitHeight()/2,iv.getImageView().getFitWidth()/2,iv.getImageView().getFitHeight()/2);//iv.getImageView().getImage().getWidth()/2,iv.getImageView().getImage().getHeight()/2, iv.getImageView().getImage().getWidth()/2,iv.getImageView().getImage().getHeight()/2
                ellipse.centerXProperty().bind(iv.getImageView().fitWidthProperty().multiply(0.5));
                ellipse.centerYProperty().bind(iv.getImageView().fitHeightProperty().multiply(0.5));
                ellipse.radiusXProperty().bind(iv.getImageView().fitWidthProperty().multiply(0.5));
                ellipse.radiusYProperty().bind(iv.getImageView().fitHeightProperty().multiply(0.5));

                iv.setImageShape(ellipse);
                iv.setShapeType("ellipse");
                break;
            case "heart":
                Circle c1 = new Circle(iv.getImageView().getImage().getHeight()/4);
                Circle c2 = new Circle(iv.getImageView().getImage().getHeight()/4);

                c1.centerXProperty().bind(iv.widthProperty().multiply(0.33));
                c1.centerYProperty().bind(iv.heightProperty().multiply(0.25));
                c1.radiusProperty().bind(iv.getImageView().fitHeightProperty().multiply(0.25));
                c2.centerXProperty().bind(iv.widthProperty().multiply(0.67));
                c2.centerYProperty().bind(iv.heightProperty().multiply(0.25));
                c2.radiusProperty().bind(iv.getImageView().fitHeightProperty().multiply(0.25));
                Shape sh = Shape.union(c1,c2);
                Polygon excess = new Polygon();
                excess.getPoints().addAll(0.0,c1.getCenterY(),
                        0.0,c1.getCenterY()+c1.getRadius(),
                        iv.getImageView().getImage().getWidth(),c1.getCenterY()+c1.getRadius(),
                        iv.getImageView().getImage().getWidth(),c1.getCenterY());
                Shape shape2 = Shape.subtract(sh,excess);
                Polygon triangle = new Polygon();
                triangle.getPoints().addAll(c1.getCenterX()-c1.getRadius(), c1.getCenterY(),
                        iv.getImageView().getImage().getWidth()/2, iv.getImageView().getImage().getHeight(),
                        c2.getCenterX()+c1.getRadius(), c1.getCenterY()); //0.0,c1.getCenterY(),iv.getImageView().getFitWidth()/2,iv.getImageView().getFitHeight(),0.0,c2.getCenterY()

                Shape finalShape = Shape.union(shape2,triangle);
                iv.setImageShape(finalShape);



                break;
            case "triangledown":
                Polygon triangleDown = new Polygon();
                triangleDown.getPoints().addAll(0.0,0.0,
                        iv.getImageView().getFitWidth()/2, iv.getImageView().getFitHeight(),
                        iv.getImageView().getFitWidth(), 0.0); //0.0,c1.getCenterY(),iv.getImageView().getFitWidth()/2,iv.getImageView().getFitHeight(),0.0,c2.getCenterY()


                iv.setImageShape(triangleDown);
                iv.setShapeType("triangledown");
                break;
            case "triangleup":
                Polygon triangleUp = new Polygon();
                triangleUp.getPoints().addAll(0.0,iv.getImageView().getFitHeight(),
                        iv.getImageView().getFitWidth()/2, 0.0,
                        iv.getImageView().getFitWidth(), iv.getImageView().getFitHeight()); //0.0,c1.getCenterY(),iv.getImageView().getFitWidth()/2,iv.getImageView().getFitHeight(),0.0,c2.getCenterY()


                iv.setImageShape(triangleUp);
                iv.setShapeType("triangleup");
                break;
        }

    }
}
