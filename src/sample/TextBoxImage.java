package sample;


import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class TextBoxImage extends ImageView {

    private String imagePath;

    public TextBoxImage(String imagePath) {
        this.imagePath = imagePath;
    }

    public TextBoxImage(Image image) {
        super(image);
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public TextBoxImage() {
        this.imagePath = imagePath;
    }
}
