package sample;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;


public class ThemeDisplay extends StackPane implements Pages {
    @FXML
    HBox images;
    @FXML
    ImageView themeFront;
    @FXML
    ImageView themeMain;
    @FXML
    ImageView themeEnd;


    private boolean isRotated;

    public boolean isRotated() {
        return isRotated;
    }

    public void setRotated(boolean rotated) {
        isRotated = rotated;
    }

    private String themeName;

    public String getThemeName() {
        return themeName;
    }

    public void setThemeName(String themeName) {
        this.themeName = themeName;
    }

    public ThemeDisplay() {
        loadUI();
    }
    public Image getFront(){
        return this.themeFront.getImage();
    }
    public Image getEnd(){
        return this.themeEnd.getImage();
    }
    public Image getMain(){
        return this.themeMain.getImage();
    }
    public ThemeDisplay(Image front, Image main, Image end) {
        loadUI();

        setImages(front,main,end);

    }


    private void setImages(Image front, Image main, Image end) {
        themeMain.setImage(main);
        themeEnd.setImage(end);
        themeFront.setImage(front);

    }
    public void setBackGround(MyPane p){
        this.setOnMouseClicked(event -> {
            if (listOfPanes.indexOf(p) == 0){
                p.addBackGroundImageWithoutZoom(this.getFront());

            }
            else if (listOfPanes.indexOf(p) == listOfPanes.size() - 1){
                p.addBackGroundImageWithoutZoom(this.getEnd());

            }

            else
            {
                p.addBackGroundImageWithoutZoom(this.getMain());

            }
            p.setTheme(this.getThemeName());
        });
    }


    private void loadUI(){
        FXMLLoader loader = new FXMLLoader(getClass().getResource("themedisplay.fxml"));
        loader.setRoot(this);
        loader.setController(this);
        try {
            loader.load();
        } catch (Exception e) {

        }
    }

}
