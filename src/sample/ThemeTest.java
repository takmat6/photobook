package sample;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

public class ThemeTest extends HBox {

    @FXML
    Group left;
    @FXML
    Group middle;
    @FXML
    Group right;

    public ThemeTest() {
        loadUI();

    }
    public void setLeft(MyPane myPane){
        left.getChildren().add(myPane);
        //left.setPrefSize(myPane.getPrefWidth(),myPane.getPrefHeight());
    }

    public void setMiddle(MyPane myPane){
        middle.getChildren().add(myPane);
        //middle.setPrefSize(myPane.getPrefWidth(),myPane.getPrefHeight());
    }
    public void setRight(MyPane myPane){
        right.getChildren().add(myPane);
        //right.setPrefSize(myPane.getPrefWidth(),myPane.getPrefHeight());
    }
    private void loadUI(){
        FXMLLoader loader = new FXMLLoader(getClass().getResource("themetest.fxml"));
        loader.setRoot(this);
        loader.setController(this);
        try {
            loader.load();
        } catch (Exception e) {

        }
    }
}
