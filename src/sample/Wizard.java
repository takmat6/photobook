package sample;

import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.Group;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Wizard extends BorderPane implements Pages,PhotoViews{

    @FXML
    Pane wizardMainPane;
    @FXML
    Pane wizardBottomPanel;
    @FXML
    VBox wizardLeftPanel;
    @FXML
    VBox wizardLeftPanelTop;
    @FXML
    VBox wizardLeftPanelMid;
    @FXML
    VBox wizardLeftPanelBottom;
    @FXML
    ScrollPane themeScrollPane;
    private int paneWidth,paneHeight;
    private double multiplier;
    private Label importedImages;
    private Button next;
    private ArrayList<MyPane> themeTest;
    private boolean themeSelected = false;
    private boolean backgroundSelected = false;
    private Color borderColor;
    private Border pageBorder;
    private Insets borderInsets;
    private BorderWidths borderWidths;
    //private String selectedTheme;
    private int maxImagesCount;
    private int minImagesCount;
    private TextField maxImages;
    private TextField minImages;
    private String selectedTheme;
    private Color bgColour;
    private ArrayList<ResizeableImageView> imagesToPane;

    private double wizardZoomfactor;

    private Label errorMessage;

    @FXML
    FlowPane themeFlow;

    private ComboBox<String> numberOfPictures;
    private BooleanProperty numberSelected = new SimpleBooleanProperty(false);


    @FXML
    private void initialize(){
        Button addPhotos = new Button("Képek importálása");
        errorMessage=new Label("");

        addPhotos.setOnAction(event -> {
            addPhotosToWizard();
        });
        wizardLeftPanelBottom.getChildren().add(addPhotos);
        importedImages=new Label();
        wizardLeftPanelTop.getChildren().add(importedImages);
        Label numbers = new Label("Képek száma oldalanként:");
        //String[] numberofpictures = {"1","1-2","2","2-3","3","3-4","4","4-5","5"};
        //numberOfPictures = new ComboBox();
        //numberOfPictures.setPromptText("Kérem válasszon ...");
        //numberOfPictures.getItems().addAll(numberofpictures);
        //numberSelected.bind(numberOfPictures.itemsProperty().isNull());
        maxImages = new TextField();
        maxImages.setPromptText("Maximum képek száma egy oldalon");
        minImages = new TextField();
        minImages.setPromptText("Minimum képek száma egy oldalon");
        numberOnly(minImages);
        numberOnly(maxImages);

        wizardLeftPanelMid.getChildren().addAll(numbers,minImages,maxImages);
        next=new Button("Tovább");
        //next.setDisable(true);
        next.setOnAction(event -> {

            if(Integer.parseInt(maxImages.getText()) >=Integer.parseInt(minImages.getText()) && Integer.parseInt(minImages.getText())>0){
                wizardLeftPanelBottom.getChildren().remove(addPhotos);
                minImagesCount=Integer.parseInt(minImages.getText());
                maxImagesCount=Integer.parseInt(maxImages.getText());
                checkPhotoViews();
                selectBackground();
            }
            else
            {
                errorMessage.setText("Kérlek add meg helyesen a paramétereket!");
            }

        });
        Platform.runLater(() -> loadThemesBackGround());
        wizardLeftPanelBottom.getChildren().add(errorMessage);
        wizardLeftPanelBottom.getChildren().add(next);


    }

    private void checkPhotoViews() {
        for(PhotoView pv : photoViews){
            if(pv.isImportant.isSelected()){
                pv.importantImage=true;
            }
            else{
                pv.importantImage=false;
            }
        }
    }

    private MyPane createMyPane(){
        MyPane myPane = new MyPane(paneWidth,paneHeight);
        wizardZoomfactor=Math.min(1100.0/((double)paneWidth*3),(680.0/(double)paneHeight));
        System.out.println("zommfactor: "+wizardZoomfactor+" 1/zoomfactor: " +(1/wizardZoomfactor));
        myPane.setZoomFactor(wizardZoomfactor);
        myPane.setPrefSize(paneWidth,paneHeight);
        myPane.setBgColor(Color.WHITE);
        //myPane.setPadding(new Insets(0,0,0,20));

        return myPane;
    }

    private void selectBackground() {
        wizardMainPane.getChildren().clear();
        //this.setCenter(null);
        Pane ThemePane = new Pane();
        ThemePane.setPrefWidth(1150);
        ThemePane.setPrefHeight(680);

        //wizardMainPane.getChildren().add(ThemePane);

        ThemeTest theme= new ThemeTest();
        themeTest = new ArrayList<>();
        for(int i = 0; i < 3 ; i++){
            themeTest.add(createMyPane());

        }

        theme.setLeft(themeTest.get(0));
        theme.setMiddle(themeTest.get(1));
        theme.setRight(themeTest.get(2));



        System.out.println("kep1 x: " +themeTest.get(0).getTranslateX()+" kep1 y: " +themeTest.get(0).getTranslateY());


        //ThemePane.getChildren().add(themeTest.get(0));
        //themeTest.get(0).setTranslateX(-520);
        //themeTest.get(0).setTranslateY(-150);
        //ThemePane.getChildren().add(themeTest.get(1));
        //themeTest.get(1).setTranslateX(-520+(themeTest.get(0).getPrefWidth()*0.5/multiplier)+10);
        //themeTest.get(1).setTranslateY(-150);
        //ThemePane.getChildren().add(themeTest.get(2));
        //themeTest.get(2).setTranslateX(-520+(themeTest.get(0).getPrefWidth()*0.5/multiplier)+20+(themeTest.get(0).getPrefWidth()*0.5/multiplier));
        //themeTest.get(2).setTranslateY(-150);



        //theme.setPrefSize(themeTest.get(0).getPrefWidth()*(0.5*multiplier),themeTest.get(0).getPrefHeight()*(0.5*multiplier));
        wizardMainPane.getChildren().add(theme);
        //wizardMainPane.

        //wizardMainPane.getChildren().add(myPane);
        //this.setCenter(myPane);


        loadThemes();




    }
    private void loadThemes(){
        //themeFlow=new FlowPane();
        loadBackgroundMenu();

        if(listOfThemes.size()>0){

            if(paneWidth<paneHeight){
                for(ThemeDisplay td : listOfThemes)
                    if(!td.isRotated())
                        themeFlow.getChildren().add(td);

            }
            else{
                for(ThemeDisplay td : listOfThemes)
                    if(td.isRotated())
                        themeFlow.getChildren().add(td);

            }
            themeFlow.setPrefSize(500*(listOfThemes.size()/2),0.0);
            themeFlow.setOrientation(Orientation.HORIZONTAL);
            themeScrollPane.setContent(themeFlow);

        }
        else {
        /*String path = getClass().getResource("themes").toString();
        //System.out.println(path);
        path = path.replace("file:/", "");
        final File dir = new File(path);
        final String[] EXTENSIONS = new String[]{"jpeg", "jpg", "png", "bmp"};
        final FilenameFilter IMAGE_FILTER = (dir1, name) -> {
            for (final String ext : EXTENSIONS) {
                if (name.endsWith("." + ext)) {
                    return (true);
                }
            }
            return (false);
        };

        if (dir.isDirectory()) {

            List<File> images = Arrays.asList(dir.listFiles(IMAGE_FILTER));



            for(int i = 0; i < images.size(); i=i+6){
                Image end = null;
                Image front=null;
                Image main=null;
                Image endRotated = null;
                Image frontRotated=null;
                Image mainRotated=null;

                if (images.get(i).toURI().toString().contains("End")) {
                    end = new Image(images.get(i).toURI().toString());
                }
                if (images.get(i + 2).toURI().toString().contains("Front")) {
                    front = new Image(images.get(i + 2).toURI().toString());
                }
                if (images.get(i + 4).toURI().toString().contains("Main")) {
                    main = new Image(images.get(i + 4).toURI().toString());
                }

                if(images.get(i+1).toURI().toString().contains("End") && images.get(i+1).toURI().toString().contains("Rotated")){
                    endRotated = new Image(images.get(i+1).toURI().toString());
                }
                if(images.get(i+3).toURI().toString().contains("Front") && images.get(i+3).toURI().toString().contains("Rotated")){
                    frontRotated = new Image(images.get(i+3).toURI().toString());
                }
                if(images.get(i+5).toURI().toString().contains("Main") && images.get(i+5).toURI().toString().contains("Rotated"))
                {
                    mainRotated = new Image(images.get(i+5).toURI().toString());
                }




                System.out.println("0." +images.get(i).toURI().toString());
                System.out.println("1." +images.get(i+1).toURI().toString());
                System.out.println("2." +images.get(i+2).toURI().toString());
                System.out.println("3." +images.get(i+3).toURI().toString());
                System.out.println("4." +images.get(i+4).toURI().toString());
                System.out.println("5." +images.get(i+5).toURI().toString());


                ThemeDisplay themeDisplay = new ThemeDisplay(front,main,end);
                themeDisplay.setRotated(false);

                ThemeDisplay themeDisplayRotated = new ThemeDisplay(frontRotated,mainRotated,endRotated);
                themeDisplayRotated.setRotated(true);
                listOfThemes.add(themeDisplay);
                listOfThemes.add(themeDisplayRotated);
                if(paneWidth>paneHeight){
                    themeFlow.getChildren().add(themeDisplayRotated);
                    themeDisplayRotated.setOnMouseClicked(event -> {
                        themeTest.get(0).addBackGroundImageWithoutZoom(themeDisplayRotated.getFront());
                        themeTest.get(1).addBackGroundImageWithoutZoom(themeDisplayRotated.getMain());
                        themeTest.get(2).addBackGroundImageWithoutZoom(themeDisplayRotated.getEnd());
                        themeSelected=true;
                    });
                }
                else{
                    themeFlow.getChildren().add(themeDisplay);
                    themeDisplay.setOnMouseClicked(event -> {
                        themeTest.get(0).addBackGroundImageWithoutZoom(themeDisplay.getFront());
                        themeTest.get(1).addBackGroundImageWithoutZoom(themeDisplay.getMain());
                        themeTest.get(2).addBackGroundImageWithoutZoom(themeDisplay.getEnd());
                        themeSelected=true;
                    });
                }




            }

            //themeScrollPane.setPrefHeight(wizardBottomPanel.getPrefHeight());
            //themeScrollPane.setPrefWidth(wizardBottomPanel.getPrefWidth());
            //wizardBottomPanel.getChildren().add(themeScrollPane);
            themeFlow.setPrefSize(500*(listOfThemes.size()/2),0.0);
            themeFlow.setOrientation(Orientation.HORIZONTAL);
            themeScrollPane.setContent(themeFlow);
            //scrollPaneFrames.setFitToWidth(true);
            //scrollPaneFrames.setFitToHeight(true);
        }

        else {
            System.out.println("nem jó a megadott útvonal");
        }*/


    }
        next.setOnAction(event -> {
            double median = (maxImagesCount+minImagesCount) /2;
            System.out.println(photoViews.size()/median);
            placeImagesOnPages();
            for(int j = listOfPanes.size()-1;j >0;j--)
            {
                if(listOfPanes.get(j).getMainPane().getChildren().toArray().length==0){
                    listOfPanes.remove(listOfPanes.get(j));
                }
            }
            if(listOfPanes.size()%2==1){
                listOfPanes.add(createNewPane());
            }
            if(!themeSelected){//if(themeTest.get(0).backGroundImageView.getImage()==null){
                for(MyPane p : listOfPanes){
                    //System.out.println(listOfPanes.size());
                    //System.out.println("not theme");
                            p.setBgColor(bgColour);
                            p.setBorderColor(borderColor);
                            p.setBorderWidth(borderWidths.getBottom());
                           // p.getBackGroundPane().setBorder(pageBorder);
                            p.setBorderInsets(borderInsets.getBottom());
                            p.getBackGroundPane().setBorder(pageBorder);

                }
            }
            else{

                int indexOfTheme=0;
                for(ThemeDisplay td : listOfThemes){
                    if(td.getThemeName()==selectedTheme){
                        indexOfTheme=listOfThemes.indexOf(td);
                    }
                }

                for(MyPane p : listOfPanes){
                    //p.setTheme(listOfThemes.get(indexOfTheme).getThemeName());
                    if(listOfPanes.indexOf(p)==0){
                        //p.setTheme(listOfThemes.get(indexOfTheme).getThemeName());
                        p.addBackGroundImageWithoutZoom(listOfThemes.get(indexOfTheme).getFront());

                    }
                    else if(listOfPanes.indexOf(p)==listOfPanes.size()-1){
                        //p.setTheme(listOfThemes.get(indexOfTheme).getThemeName());
                        p.addBackGroundImageWithoutZoom(listOfThemes.get(indexOfTheme).getEnd());
                    }
                    else{
                        //p.setTheme(listOfThemes.get(indexOfTheme).getThemeName());
                        p.addBackGroundImageWithoutZoom(listOfThemes.get(indexOfTheme).getMain());
                    }

                    p.setBgColor(null);

                    p.setTheme(listOfThemes.get(indexOfTheme).getThemeName());

                }
            }
            wizardBottomPanel.setVisible(false);
            wizardMainPane.getChildren().clear();
            wizardLeftPanelMid.getChildren().clear();
            ScrollPane checkScrollpane = new ScrollPane();
            FlowPane checkFlowPane = new FlowPane();

            for(MyPane p : listOfPanes){
                Group g = new Group();
                g.getChildren().add(p);
                checkFlowPane.getChildren().add(g);
            }
            checkFlowPane.setVgap(10);
            checkFlowPane.setHgap(10);
            checkFlowPane.setPrefSize((listOfPanes.size()*(listOfPanes.get(0).getPrefWidth()+20))*1/multiplier,0.0);
            checkFlowPane.setMaxSize((listOfPanes.size()*(listOfPanes.get(0).getPrefWidth()+20))*1/multiplier,730);//listOfPanes.get(0).getPrefHeight()*1/multiplier
            checkFlowPane.setOrientation(Orientation.VERTICAL);
            checkScrollpane.setContent(checkFlowPane);
            checkScrollpane.setPrefSize(1100,730.0);
            checkScrollpane.setMaxSize(1100,730.0);
            //checkFlowPane.setPrefSize(1100,0.0);
            //checkFlowPane.setMaxSize(1100,0.0);
            wizardMainPane.getChildren().add(checkScrollpane);
            next.setVisible(false);


        });
    }
    //iv.setImageBorder(new Border(new BorderStroke(Color.color(Math.random(),Math.random(),Math.random()), BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(24,24,24,24), new Insets(0, 0, 0, 0))));
    private void resizeImage(ResizeableImageView iv,int imageCount){
        if(iv.isImportant()){
            if(iv.getImageView().getFitWidth()> iv.getImageView().getFitHeight()) // ha fekvő kép
            {
                iv.getImageView().setFitWidth( paneWidth*(0.7));
            }
            else
            {
                iv.getImageView().setFitHeight(paneHeight*(0.7));
            }
        }
        else
        {
            if(iv.getImageView().getImage().getWidth()> iv.getImageView().getImage().getHeight()) // ha fekvő kép
            {
                iv.getImageView().setFitWidth(paneWidth/imageCount);
            }
            else
            {
                iv.getImageView().setFitHeight(paneHeight/imageCount);
            }
        }
    }
    private void addLayout(List<ResizeableImageView> arrayList,MyPane pane){
        Random rando =new Random();
        int X=0;
        int Y=0;
        System.out.println("képek száma: "+arrayList.size());
        for(ResizeableImageView iv : arrayList){
            resizeImage(iv,arrayList.size());
        }
        switch(arrayList.size()){
            case 1:
                X=0;
                Y=0;
                pane.placeImageViewOnPane(arrayList.get(0));
                X = (int)((pane.getPaneWidth()/2)-(arrayList.get(0).getImageView().getFitWidth()/2));
                Y = (int)((pane.getPaneHeight()/2)-(arrayList.get(0).getImageView().getFitHeight()/2));
                arrayList.get(0).setTranslateX(X);
                arrayList.get(0).setTranslateY(Y);

                break;
            case 2:
                int r = rando.ints(0,2).findFirst().getAsInt();
                X=0;
                Y=0;
                if(r==0 || r==1)
                {
                    pane.placeImageViewOnPane(arrayList.get(0));
                    X = (int)((pane.getPaneWidth()/2)-(arrayList.get(0).getImageView().getFitWidth()/2));
                    Y = rando.ints(20,51).findFirst().getAsInt();//(int)((pane.getPaneHeight()/2)-(arrayList.get(0).getImageView().getFitHeight()/2));
                    arrayList.get(0).setTranslateX(X);
                    arrayList.get(0).setTranslateY(Y);
                    pane.placeImageViewOnPane(arrayList.get(1));
                    X = (int)((pane.getPaneWidth()/2)-(arrayList.get(1).getImageView().getFitWidth()/2));
                    Y = pane.getPaneHeight()-(int)arrayList.get(1).getImageView().getFitHeight()-rando.ints(20,50).findFirst().getAsInt() ;
                    arrayList.get(1).setTranslateX(X);
                    arrayList.get(1).setTranslateY(Y);
                }
                else{


                }

                break;
            case 3:

                pane.placeImageViewOnPane(arrayList.get(0));
                X = rando.ints(20,51).findFirst().getAsInt();
                Y = rando.ints(20,51).findFirst().getAsInt();
                arrayList.get(0).setTranslateX(X);
                arrayList.get(0).setTranslateY(Y);


                pane.placeImageViewOnPane(arrayList.get(1));
                X = pane.getPaneWidth()-(int)(50+(arrayList.get(1).getImageView().getFitWidth()));
                Y = (int)(arrayList.get(0).getImageView().getFitHeight()*0.95);
                arrayList.get(1).setTranslateX(X);
                arrayList.get(1).setTranslateY(Y);


                pane.placeImageViewOnPane(arrayList.get(2));
                X = rando.ints(20,51).findFirst().getAsInt();//rando.ints(20,(int)(arrayList.get(2).getImageView().getFitWidth())).findFirst().getAsInt();
                Y = pane.getPaneHeight()-(int)arrayList.get(2).getImageView().getFitHeight()-rando.ints(20,51).findFirst().getAsInt();
                arrayList.get(2).setTranslateX(X);
                arrayList.get(2).setTranslateY(Y);
                break;
            case 4:

                pane.placeImageViewOnPane(arrayList.get(0));
                X = rando.ints(20,50).findFirst().getAsInt();
                Y = rando.ints(20,51).findFirst().getAsInt();
                arrayList.get(0).setTranslateX(X);
                arrayList.get(0).setTranslateY(Y);


                pane.placeImageViewOnPane(arrayList.get(1));
                X = pane.getPaneWidth()-((int)((arrayList.get(1).getImageView().getFitWidth()))+rando.ints(20,51).findFirst().getAsInt());
                Y = (int)(arrayList.get(0).getImageView().getFitHeight()*0.95);
                arrayList.get(1).setTranslateX(X);
                arrayList.get(1).setTranslateY(Y);

                pane.placeImageViewOnPane(arrayList.get(2));
                X = rando.ints(20,50).findFirst().getAsInt();//pane.getPaneWidth()-(int)((arrayList.get(1).getImageView().getFitWidth())-rando.ints(20,51).findFirst().getAsInt());
                Y = (int)(arrayList.get(1).getImageView().getFitHeight()*0.95);
                arrayList.get(2).setTranslateX(X);
                arrayList.get(2).setTranslateY(Y);


                pane.placeImageViewOnPane(arrayList.get(3));
                X = rando.ints(20,(int)(arrayList.get(3).getImageView().getFitWidth()-50)).findFirst().getAsInt();
                Y = pane.getPaneHeight()-(int)arrayList.get(2).getImageView().getFitHeight()-rando.ints(20,51).findFirst().getAsInt();
                arrayList.get(3).setTranslateX(X);
                arrayList.get(3).setTranslateY(Y);
                break;

            case 5:

                pane.placeImageViewOnPane(arrayList.get(0));
                X = rando.ints(20,50).findFirst().getAsInt();
                Y = rando.ints(20,51).findFirst().getAsInt();
                arrayList.get(0).setTranslateX(X);
                arrayList.get(0).setTranslateY(Y);


                pane.placeImageViewOnPane(arrayList.get(1));
                X = pane.getPaneWidth()-((int)((arrayList.get(1).getImageView().getFitWidth()))+rando.ints(20,51).findFirst().getAsInt());
                Y = (int)(arrayList.get(0).getImageView().getFitHeight()*0.95);
                arrayList.get(1).setTranslateX(X);
                arrayList.get(1).setTranslateY(Y);

                pane.placeImageViewOnPane(arrayList.get(2));
                X = rando.ints(20,50).findFirst().getAsInt();//pane.getPaneWidth()-(int)((arrayList.get(1).getImageView().getFitWidth())-rando.ints(20,51).findFirst().getAsInt());
                Y = (int)(arrayList.get(1).getImageView().getFitHeight()*0.95);
                arrayList.get(2).setTranslateX(X);
                arrayList.get(2).setTranslateY(Y);


                pane.placeImageViewOnPane(arrayList.get(3));
                X = rando.ints(20,(int)(arrayList.get(3).getImageView().getFitWidth()-50)).findFirst().getAsInt();
                Y = pane.getPaneHeight()-(int)arrayList.get(2).getImageView().getFitHeight()-rando.ints(20,51).findFirst().getAsInt();
                arrayList.get(3).setTranslateX(X);
                arrayList.get(3).setTranslateY(Y);

                pane.placeImageViewOnPane(arrayList.get(4));
                X = rando.ints(20,50).findFirst().getAsInt();//pane.getPaneWidth()-(int)((arrayList.get(1).getImageView().getFitWidth())-rando.ints(20,51).findFirst().getAsInt());
                Y = (int)(arrayList.get(3).getImageView().getFitHeight()*0.95);
                arrayList.get(4).setTranslateX(X);
                arrayList.get(4).setTranslateY(Y);
                break;
            case 6:

                pane.placeImageViewOnPane(arrayList.get(0));
                X = rando.ints(20,50).findFirst().getAsInt();
                Y = rando.ints(20,51).findFirst().getAsInt();
                arrayList.get(0).setTranslateX(X);
                arrayList.get(0).setTranslateY(Y);


                pane.placeImageViewOnPane(arrayList.get(1));
                X = pane.getPaneWidth()-((int)((arrayList.get(1).getImageView().getFitWidth()))+rando.ints(20,51).findFirst().getAsInt());
                Y = (int)(arrayList.get(0).getImageView().getFitHeight()*0.95);
                arrayList.get(1).setTranslateX(X);
                arrayList.get(1).setTranslateY(Y);

                pane.placeImageViewOnPane(arrayList.get(2));
                X = rando.ints(20,50).findFirst().getAsInt();//pane.getPaneWidth()-(int)((arrayList.get(1).getImageView().getFitWidth())-rando.ints(20,51).findFirst().getAsInt());
                Y = (int)(arrayList.get(1).getImageView().getFitHeight()*0.95);
                arrayList.get(2).setTranslateX(X);
                arrayList.get(2).setTranslateY(Y);


                pane.placeImageViewOnPane(arrayList.get(3));
                X = rando.ints(20,(int)(arrayList.get(3).getImageView().getFitWidth()-50)).findFirst().getAsInt();
                Y = pane.getPaneHeight()-(int)arrayList.get(2).getImageView().getFitHeight()-rando.ints(20,51).findFirst().getAsInt();
                arrayList.get(3).setTranslateX(X);
                arrayList.get(3).setTranslateY(Y);

                pane.placeImageViewOnPane(arrayList.get(4));
                X = rando.ints(20,50).findFirst().getAsInt();//pane.getPaneWidth()-(int)((arrayList.get(1).getImageView().getFitWidth())-rando.ints(20,51).findFirst().getAsInt());
                Y = (int)(arrayList.get(3).getImageView().getFitHeight()*0.95);
                arrayList.get(4).setTranslateX(X);
                arrayList.get(4).setTranslateY(Y);

                pane.placeImageViewOnPane(arrayList.get(5));
                X = rando.ints(20,(int)(arrayList.get(5).getImageView().getFitWidth()-50)).findFirst().getAsInt();
                Y = pane.getPaneHeight()-(int)arrayList.get(4).getImageView().getFitHeight()-rando.ints(20,51).findFirst().getAsInt();
                arrayList.get(5).setTranslateX(X);
                arrayList.get(5).setTranslateY(Y);
                break;

        }
    }

    private void placeImagesOnPages() {
        Random rando = new Random();
        int images;
        imagesToPane = new ArrayList<>();
        for(int i = 0; i < photoViews.size();i++){
            ResizeableImageView iv = new ResizeableImageView(photoViews.get(i).getImage());
            System.out.println(photoViews.get(i).isImportantImage());

            iv.setImportant(photoViews.get(i).isImportantImage());

            //resizeImage(iv,photoViews.get(i).isImportantImage());


            iv.setPathOfImage(photoViews.get(i).getPathOfFile());
            imagesToPane.add(iv);
        }
        int minImages =0;
        for(int i = 0;i < listOfPanes.size();i++){

            images = rando.ints(minImagesCount,maxImagesCount+1).findFirst().getAsInt();
            System.out.println("random szám: "+images);
            if(imagesToPane.size()-(minImages)<0)
            {
                break;
            }
            if((imagesToPane.size()-(minImages)) >= images)
            {
                addLayout(imagesToPane.subList(minImages, minImages + images), listOfPanes.get(i));
                if(i<listOfPanes.size()-1){
                    addTwoPanes();
                }
            }
            else {
                addLayout(imagesToPane.subList(minImages,imagesToPane.size()),listOfPanes.get(i));
            }
            minImages=minImages+images;
        }







    }

    private void addTwoPanes() {
        listOfPanes.add(createNewPane());
        listOfPanes.add(createNewPane());
        //numberOfPages+=2;
        //setTextFieldText(currentPage,numberOfPages);
    }
    private MyPane createNewPane(){
        MyPane p = new MyPane(paneWidth,paneHeight);
        p.setZoomFactor( Math.min((625.0/(double)paneWidth),(700.0/(double)paneHeight)));
        p.setBgColor(Color.WHITE);
        /*
        p.setStyle("-fx-background-color: white;"

        */
        p.setPrefSize(paneWidth,paneHeight);
        return p;
    }

    private void loadBackgroundMenu() {
        VBox bgMenu = new VBox();
        Label border = new Label("Szegély vastagsága:");
        Slider borderSlider = new Slider();
        borderSlider.setBlockIncrement(1.0);
        borderSlider.setShowTickLabels(true);
        borderSlider.setShowTickMarks(true);
        borderSlider.setMajorTickUnit(1.0);
        borderSlider.setMax(25);
        borderSlider.setMin(0.0);
        borderSlider.setSnapToTicks(true);
        Label margin = new Label("Margók:");
        Slider marginSlider = new Slider();
        marginSlider.setBlockIncrement(1.0);
        marginSlider.setShowTickLabels(true);
        marginSlider.setShowTickMarks(true);
        marginSlider.setMajorTickUnit(1.0);
        marginSlider.setMax(25);
        marginSlider.setSnapToTicks(true);
        ColorPicker brdrColor =new ColorPicker();
        Label bordercolor = new Label("Szegély háttérszíne:");

        ColorPicker bgColor =new ColorPicker();
        Label bgcolor = new Label("Oldal háttérszíne:");
        bgColor.setEditable(true);
        bgColor.setPrefSize(133,27);
        bgColor.setPadding(new Insets(10,10,10,10));

        //Button setBackground = new Button("Háttérszín beállítása az oldalakra");
        bgMenu.getChildren().addAll(border,borderSlider,margin,marginSlider,bordercolor,brdrColor,bgcolor,bgColor); //,setBackground

        //setBackground.setOnAction(event -> {
         //   addBGColor();
        //});
        bgColor.setOnAction(event -> {

            for(MyPane p : themeTest){
                if(themeSelected){//if(p.backGroundImageView.getImage()!=null){
                    p.clearBackground();
                }
                themeSelected=false;
                bgColour=bgColor.getValue();
                p.setBgColor(bgColor.getValue());
            }
        });
        brdrColor.valueProperty().addListener(((observable, oldValue, newValue) -> {
            //System.out.println(newValue.toString());
            borderColor=newValue;
            pageBorder=new Border(new BorderStroke(newValue,BorderStrokeStyle.SOLID,CornerRadii.EMPTY,borderWidths,borderInsets));
            for(MyPane p : themeTest){
                if(themeSelected){//if(p.backGroundImageView.getImage()!=null){
                    p.clearBackground();
                }
                themeSelected=false;
                p.getBackGroundPane().setBorder(pageBorder);
                p.setBorderColor(newValue);
            }
        }));
        borderSlider.valueProperty().addListener(((observable, oldValue, newValue) -> {
            //System.out.println(newValue.doubleValue());

            borderWidths=new BorderWidths(newValue.doubleValue(),newValue.doubleValue(),newValue.doubleValue(),newValue.doubleValue());
            pageBorder = new Border(new BorderStroke(borderColor,BorderStrokeStyle.SOLID,CornerRadii.EMPTY,borderWidths,borderInsets));
            for(MyPane p : themeTest){
                if(themeSelected){//if(p.backGroundImageView.getImage()!=null){
                    p.clearBackground();
                }
                themeSelected=false;
                p.setBorderWidth(newValue.doubleValue());
                p.getBackGroundPane().setBorder(pageBorder);
            }


        }));
        marginSlider.valueProperty().addListener(((observable, oldValue, newValue) -> {
            borderInsets=new Insets(newValue.doubleValue(),newValue.doubleValue(),newValue.doubleValue(),newValue.doubleValue());
            pageBorder = new Border(new BorderStroke(borderColor,BorderStrokeStyle.SOLID,CornerRadii.EMPTY,borderWidths,borderInsets));
            for(MyPane p : themeTest){
               if(themeSelected){// if(p.backGroundImageView.getImage()!=null){
                    p.clearBackground();
                }
                System.out.println(themeTest.size());
                themeSelected=false;
                p.setBorderInsets(newValue.doubleValue());
                p.getBackGroundPane().setBorder(pageBorder);
            }


        }));

        wizardLeftPanelMid.getChildren().clear();
        wizardLeftPanelMid.getChildren().add(bgMenu);



    }



    private void loadThemesBackGround(){
        Task<Void> loadThemesThread = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                String path = getClass().getResource("themes").toString();
                //System.out.println(path);
                path = path.replace("file:/", "");
                final File dir = new File(path);
                final String[] EXTENSIONS = new String[]{"jpeg", "jpg", "png", "bmp"};
                final FilenameFilter IMAGE_FILTER = (dir1, name) -> {
                    for (final String ext : EXTENSIONS) {
                        if (name.endsWith("." + ext)) {
                            return (true);
                        }
                    }
                    return (false);
                };

                if (dir.isDirectory()) {

                    List<File> images = Arrays.asList(dir.listFiles(IMAGE_FILTER));



                    for(int i = 0; i < images.size(); i=i+6){
                        Image end = null;
                        Image front=null;
                        Image main=null;
                        Image endRotated = null;
                        Image frontRotated=null;
                        Image mainRotated=null;

                        if (images.get(i).toURI().toString().contains("End")) {
                            end = new Image(images.get(i).toURI().toString());
                        }
                        if (images.get(i + 2).toURI().toString().contains("Front")) {
                            front = new Image(images.get(i + 2).toURI().toString());
                        }
                        if (images.get(i + 4).toURI().toString().contains("Main")) {
                            main = new Image(images.get(i + 4).toURI().toString());
                        }

                        if(images.get(i+1).toURI().toString().contains("End") && images.get(i+1).toURI().toString().contains("Rotated")){
                            endRotated = new Image(images.get(i+1).toURI().toString());
                        }
                        if(images.get(i+3).toURI().toString().contains("Front") && images.get(i+3).toURI().toString().contains("Rotated")){
                            frontRotated = new Image(images.get(i+3).toURI().toString());
                        }
                        if(images.get(i+5).toURI().toString().contains("Main") && images.get(i+5).toURI().toString().contains("Rotated"))
                        {
                            mainRotated = new Image(images.get(i+5).toURI().toString());
                        }




                        System.out.println("0." +images.get(i).toURI().toString());
                        System.out.println("1." +images.get(i+1).toURI().toString());
                        System.out.println("2." +images.get(i+2).toURI().toString());
                        System.out.println("3." +images.get(i+3).toURI().toString());
                        System.out.println("4." +images.get(i+4).toURI().toString());
                        System.out.println("5." +images.get(i+5).toURI().toString());


                        ThemeDisplay themeDisplay = new ThemeDisplay(front,main,end);
                        themeDisplay.setRotated(false);



                        ThemeDisplay themeDisplayRotated = new ThemeDisplay(frontRotated,mainRotated,endRotated);
                        themeDisplayRotated.setRotated(true);
                        listOfThemes.add(themeDisplay);
                        listOfThemes.add(themeDisplayRotated);



                        String temp =(images.get(i).toURI().toString().replace("End", "").replace("file:",""));
                        String[] temp2 = temp.split("/");

                        themeDisplay.setThemeName(temp2[temp2.length-1].replace(".png",""));
                        System.out.println(themeDisplay.getThemeName());
                        temp =(images.get(i).toURI().toString().replace("End", "").replace("file:",""));
                        temp2 = temp.split("/");
                        themeDisplayRotated.setThemeName(temp2[temp2.length-1].replace(".png","")+"Rotated");
                        if(paneWidth>paneHeight){
                            //themeFlow.getChildren().add(themeDisplayRotated);
                            themeDisplayRotated.setOnMouseClicked(event -> {
                                themeTest.get(0).addBackGroundImageWithoutZoom(themeDisplayRotated.getFront());
                                themeTest.get(1).addBackGroundImageWithoutZoom(themeDisplayRotated.getMain());
                                themeTest.get(2).addBackGroundImageWithoutZoom(themeDisplayRotated.getEnd());
                                themeSelected=true;
                                selectedTheme = themeDisplayRotated.getThemeName();
                                System.out.println(themeDisplayRotated.getThemeName());
                            });
                        }
                        else{
                            //themeFlow.getChildren().add(themeDisplay);
                            themeDisplay.setOnMouseClicked(event -> {
                                themeTest.get(0).addBackGroundImageWithoutZoom(themeDisplay.getFront());
                                themeTest.get(1).addBackGroundImageWithoutZoom(themeDisplay.getMain());
                                themeTest.get(2).addBackGroundImageWithoutZoom(themeDisplay.getEnd());
                                themeSelected=true;
                                selectedTheme = themeDisplay.getThemeName();
                                System.out.println(themeDisplay.getThemeName());
                            });
                        }




                    }
                }
                return null;
            }
        };

        new Thread(loadThemesThread).start();


    }


    public void initData(int paneHeight,int paneWidth,double multiplier){
        this.paneHeight=paneHeight;
        this.paneWidth = paneWidth;
        this.multiplier=multiplier;
    }

    private void addPhotosToWizard() {
        FileChooser fileChooser = new FileChooser();
        Stage stage = new Stage();
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Képek","*.png","*.jpeg","*.jpg","*.bmp")

        );
        List<File> list =
                fileChooser.showOpenMultipleDialog(stage);
        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setPrefWidth(1100);
        scrollPane.setPrefHeight(680);
        FlowPane flowPane = new FlowPane();
        flowPane.setHgap(20);
        flowPane.setVgap(20);
        flowPane.setPrefWrapLength(1100);
        flowPane.setPadding(new Insets(10,10,10,10));

            if(!list.isEmpty()){
                for(File file : list){
                int id=0;
                if(fileID.isEmpty()){

                }
                else{
                    id=(fileID.get(fileID.size()-1))+1;
                }

                PhotoView photoView = new PhotoView(id);
                fileID.add(id);
                Image image = new Image(file.toURI().toString() );
                if(image.getWidth() > paneWidth || image.getHeight() > paneHeight){
                    image = new Image(file.toURI().toString(),paneWidth-(paneWidth/10),paneHeight-(paneHeight/10),true,true,true);
                }


                photoView.setPathOfFile(file.getPath());
                photoView.setPhoto(image);
                photoView.setImportantVisibility(true);

                photoViews.add(photoView);


                    photoView.setOnContextMenuRequested(event -> {
                        ImageViewContextMenu contextMenu = new ImageViewContextMenu(photoView,flowPane);
                        contextMenu.show(photoView,event.getScreenX(),event.getScreenY());
                    });

                flowPane.getChildren().add(photoView);


            }
                flowPane.setOrientation(Orientation.HORIZONTAL);
                scrollPane.setContent(flowPane);
                wizardMainPane.getChildren().add(scrollPane);

        }
            importedImages.setText("Importált képek száma: "+photoViews.size());
            if(next.isDisabled()){
                if(!(Integer.parseInt(minImages.getText()) > 0 && Integer.parseInt(maxImages.getText())<=10))
                {

                }
                else{
                    next.setDisable(false);
                }


            }

            //TextField numberOfPictures = new TextField();
            //numberOfPictures.setPromptText("Képek száma egy oldalon");

    }
    private class ImageViewContextMenu extends ContextMenu {
        public ImageViewContextMenu(PhotoView iv,FlowPane flowPane) {
            MenuItem deleteImageView = new MenuItem("Kép törlése");
            deleteImageView.setOnAction(event -> {

                deleteImage(iv,flowPane);


            });

            MenuItem addToGroup = new MenuItem("Hozzáadás csoporthoz");
            addToGroup.setOnAction(event -> {
                try {
                    //addToGroup(iv);
                }
                catch (Exception e){
                    System.out.println("Hiba a csoportnál");
                    System.out.println(e.toString());
                }

            });



            getItems().addAll(deleteImageView); //addToGroup,new SeparatorMenuItem(),
            //getItems().add(resize);
        }
    }

    private void deleteImage(PhotoView iv,FlowPane flowPane) {
        /*for(PhotoView pv :photoViews){
            if(pv.equals(iv)){
                photoViews.remove(pv);
            }
        }*/
        photoViews.remove(iv);
        flowPane.getChildren().remove(iv);
        importedImages.setText("Importált képek száma: "+photoViews.size());


    }

    private void numberOnly(TextField tf){
        tf.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d*")) {
                tf.setText(newValue.replaceAll("[^\\d]", ""));
            }
        });
    }

}
